package net.avcompris.commons.query;

import org.joda.time.DateTime;

public interface Filterings<T extends Filtering<U>, U extends Filtering.Field> {

	T parse(String expression) throws FilterSyntaxException;

	// ======== ALWAYS TRUE|FALSE

	T alwaysTrue();

	T alwaysFalse();

	// ======== GENERIC COMPARISONS

	T eq(U field, Arg arg);

	T neq(U field, Arg arg);

	T gt(U field, Arg arg);

	T gte(U field, Arg arg);

	T lt(U field, Arg arg);

	T lte(U field, Arg arg);

	T contains(U field, Arg arg);

	T doesntContain(U field, Arg arg);

	// ======== STRING COMPARISONS

	T eq(U field, String s);

	T neq(U field, String s);

	T contains(U field, String s);

	T doesntContain(U field, String s);

	// ======== INT COMPARISONS

	T eq(U field, int n);

	T neq(U field, int n);

	T gt(U field, int n);

	T gte(U field, int n);

	T lt(U field, int n);

	T lte(U field, int n);

	// ======== BOOLEAN COMPARISONS

	T eq(U field, boolean b);

	T neq(U field, boolean b);

	// ======== ENUM COMPARISONS

	T eq(U field, Enum<?> b);

	T neq(U field, Enum<?> b);

	// ======== DATETIME COMPARISONS

	T eq(U field, DateTime dateTime);

	T gt(U field, DateTime dateTime);

	T gte(U field, DateTime dateTime);

	T lt(U field, DateTime dateTime);

	T lte(U field, DateTime dateTime);
}
