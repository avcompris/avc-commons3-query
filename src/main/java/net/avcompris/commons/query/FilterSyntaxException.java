package net.avcompris.commons.query;

import javax.annotation.Nullable;

public final class FilterSyntaxException extends Exception {

	private static final long serialVersionUID = 3274674821374433985L;

	public FilterSyntaxException(@Nullable final String message) {

		super(message);
	}

	public FilterSyntaxException(@Nullable final String message, @Nullable final Throwable cause) {

		super(message, cause);
	}
}
