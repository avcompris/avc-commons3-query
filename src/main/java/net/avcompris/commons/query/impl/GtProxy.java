package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.Filtering.Type.GT;
import static net.avcompris.commons.query.impl.FieldUtils.isDateTimeField;
import static net.avcompris.commons.query.impl.FieldUtils.isIntField;
import static net.avcompris.commons.query.impl.FilteringFieldProxy.extractFieldValue;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;

import net.avcompris.commons.query.DateTimePrecision;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.FilteringHandler;

final class GtProxy<T extends Filtering<U>, U extends Filtering.Field> extends AbstractFilteringFieldProxy<T, U> {

	public GtProxy(final Class<? extends T> filteringClass, final U field, @Nullable final Object refValue) {

		super(filteringClass, field, "gt", refValue);
	}

	@Override
	public boolean match(final Object arg) {

		if (arg == null && refValue == null) {

			return false;
		}

		final Class<?> argClass = arg.getClass();

		if (Integer.class.equals(argClass)) {

			return (int) arg > (int) refValue;

		} else if (refValue instanceof Integer) {

			final int value = extractFieldValue(arg, field, int.class);

			return value > (int) refValue;

		} else if (DateTime.class.equals(argClass)) {

			return ((DateTime) arg).isAfter((DateTime) refValue);

		} else if (refValue instanceof DateTime) {

			final DateTime value = extractFieldValue(arg, field, DateTime.class);

			return value.isAfter((DateTime) refValue);

		} else if (refValue instanceof DateTimeArg) {

			final DateTime value = extractFieldValue(arg, field, DateTime.class);

			final DateTimeArg dateTimeArg = (DateTimeArg) refValue;

			return dateTimeArg.gt(value);
		}

		throw new NotImplementedException("argClass: " + argClass);
	}

	@Override
	public void applyTo(final FilteringHandler<U> handler) {

		if (isIntField(field)) {

			handler.gt(field, (int) refValue);

		} else if (isDateTimeField(field)) {

			if (refValue instanceof DateTimeArg) {

				final DateTimeArg dateTimeArg = (DateTimeArg) refValue;

				handler.gt(field, dateTimeArg.dateTime, dateTimeArg.precision);

			} else {

				handler.gt(field, (DateTime) refValue, DateTimePrecision.MILLISECOND);
			}

		} else {

			throw new NotImplementedException("refValue.class: " + refValue.getClass().getName());
		}
	}

	@Override
	public Type getType() {

		return GT;
	}
}
