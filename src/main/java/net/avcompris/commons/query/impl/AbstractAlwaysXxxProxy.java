package net.avcompris.commons.query.impl;

import javax.annotation.Nullable;

import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.FilteringHandler;

abstract class AbstractAlwaysXxxProxy<T extends Filtering<U>, U extends Filtering.Field>
		extends AbstractFilteringProxy<T, U> {

	private final boolean value;

	protected AbstractAlwaysXxxProxy(final Class<? extends T> filteringClass, final boolean value) {

		super(filteringClass);

		this.value = value;
	}

	@Override
	public boolean match(final Object arg) {

		return value;
	}

	@Override
	public void applyTo(final FilteringHandler<U> handler) {

		if (value) {

			handler.setTrue();

		} else {

			handler.setFalse();
		}
	}

	@Override
	public final int hashCode() {

		return filteringClass.hashCode() //
				+ (value ? 1 : 0);
	}

	@Override
	public final boolean equals(@Nullable final Object arg) {

		if (arg == null || !(arg instanceof FilteringProxy) || !(arg instanceof AbstractAlwaysXxxProxy)) {

			return false;
		}

		@SuppressWarnings("unchecked")
		final FilteringProxy<T, U> filteringProxy = (FilteringProxy<T, U>) arg;

		if (!filteringClass.equals(filteringProxy.getFilteringClass())) {

			return false;
		}

		@SuppressWarnings("unchecked")
		final AbstractAlwaysXxxProxy<T, U> refProxy = (AbstractAlwaysXxxProxy<T, U>) arg;

		final boolean refValue = refProxy.value;

		return value == refValue;
	}

	@Override
	public final String toString() {

		return Boolean.toString(value);
	}
}
