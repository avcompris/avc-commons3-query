package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.Filtering.Type.OR;

import java.util.Arrays;

import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.FilteringHandler;
import net.avcompris.commons.query.Or;

final class OrProxy<T extends Filtering<U>, U extends Filtering.Field> extends AbstractConnectorProxy<T, U>
		implements Or<U> {

	public OrProxy(final Class<? extends T> filteringClass, final Filtering<U>[] fs) {

		super(filteringClass, "or", fs);
	}

	public OrProxy(final Class<? extends T> filteringClass, final Filtering<U> f1, final Filtering<U> f2) {

		super(filteringClass, "or", f1, f2);
	}

	public OrProxy(final Class<? extends T> filteringClass, final Filtering<U> f, final Filtering<U>[] fs) {

		super(filteringClass, "or", f, fs);
	}

	public OrProxy(final Class<? extends T> filteringClass, final Filtering<U>[] fs1, final Filtering<U>[] fs2) {

		super(filteringClass, "or", fs1, fs2);
	}

	public OrProxy(final Class<? extends T> filteringClass, final Filtering<U>[] fs, final Filtering<U> f) {

		super(filteringClass, "or", fs, f);
	}

	@Override
	public boolean match(final Object arg) {

		for (final Filtering<U> f : fs) {

			if (f.match(arg)) {

				return true;
			}
		}

		return false;
	}

	@Override
	public void applyTo(final FilteringHandler<U> handler) {

		for (final Filtering<U> f : fs) {

			final FilteringHandler<U> subHandler = handler.newOrMember();

			f.applyTo(subHandler);
		}
	}

	@Override
	public Type getType() {

		return OR;
	}

	@Override
	public Filtering<U>[] getArgs() {

		return Arrays.copyOf(fs, fs.length);
	}
}
