package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Locale.ENGLISH;

import javax.annotation.Nullable;

import net.avcompris.commons.query.Arg;
import net.avcompris.commons.query.FilterSemanticsException;
import net.avcompris.commons.query.FilterSyntaxException;

final class EnumArg implements Arg {

	public final Class<? extends Enum<?>> enumClass;

	public final String s;

	@Nullable
	private final Enum<?> enumValue;

	public EnumArg(final Class<? extends Enum<?>> enumClass, final String s) throws FilterSyntaxException {

		this.enumClass = checkNotNull(enumClass, "enumClass");

		this.s = checkNotNull(s, "s");

		final String lowercase = s.toLowerCase(ENGLISH);

		Enum<?> enumValue = null;

		for (final Enum<?> enumConstant : enumClass.getEnumConstants()) {

			if (lowercase.contentEquals(enumConstant.name().toLowerCase(ENGLISH))) {

				enumValue = enumConstant;

				break;
			}
		}

		this.enumValue = enumValue;
	}

	public boolean hasEnumValue() {

		return enumValue != null;
	}

	public Enum<?> enumValue() {

		if (enumValue == null) {
			throw new FilterSemanticsException("Cannot parse enumValue: " + s + " (" + enumClass.getName() + ")");
		}

		return enumValue;
	}
}
