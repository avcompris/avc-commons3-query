package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import net.avcompris.commons.query.DateTimePrecision;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.FilteringHandler;

public final class SqlWhereClause {

	private final String sql;

	private static SqlWhereClause EMPTY = new SqlWhereClause("");

	private SqlWhereClause(final String sql) {

		this.sql = checkNotNull(sql, "sql");
	}

	public static <U extends Filtering.Field> SqlWhereClause build(@Nullable final Filtering<U> filtering,
			final Class<U> fieldClass) {

		return build(filtering, fieldClass, null);
	}

	public static <U extends Filtering.Field> SqlWhereClause build(@Nullable final Filtering<U> filtering,
			final Class<U> fieldClass, @Nullable final String prefix) {

		if (filtering == null) {

			return EMPTY;
		}

		final SqlBuilder<U> sqlBuilder = new SqlBuilder<U>(fieldClass, //
				isBlank(prefix) ? "" : prefix);

		filtering.applyTo(sqlBuilder);

		return new SqlWhereClause(sqlBuilder.build());
	}

	/**
	 * @param prefix e.g. " WHERE", or " AND"
	 */
	public String getSQL(final String prefix) {

		checkNotNull(prefix, "prefix");

		return isBlank(sql) //
				? "" //
				: isBlank(prefix) //
						? sql //
						: prefix + " " + sql;
	}

	public void setParameters(final PreparedStatement pstmt, final int parmIndex) throws SQLException {

		checkNotNull(pstmt, "pstmt");
	}

	private static final class FieldSpec {

		public final String sqlName;

		public FieldSpec(final String sqlName) {

			this.sqlName = checkNotNull(sqlName, "sqlName");
		}
	}

	private static class SqlBuilder<U extends Filtering.Field> implements FilteringHandler<U> {

		// private String sql = "";

		private static final Set<Class<? extends Filtering.Field>> CLASSES_WITH_FIELD_DESCS = newHashSet();
		private static final Map<Filtering.Field, FieldSpec> FIELD_SPECS = newHashMap();

		private final String prefix;

		private final Class<U> fieldClass;

		private String sqlClause = null;
		// private Type type = null;
		private SqlBuilder<U> notMember = null;
		private final List<SqlBuilder<U>> orMembers = new ArrayList<>();
		private final List<SqlBuilder<U>> andMembers = new ArrayList<>();

		private SqlBuilder(final Class<U> fieldClass, final String prefix) {

			this.fieldClass = checkNotNull(fieldClass, "fieldClass");
			this.prefix = checkNotNull(prefix, "prefix");

			checkArgument(fieldClass.isEnum(), //
					"fieldClass should be enum: %s", fieldClass.getName());

			if (!CLASSES_WITH_FIELD_DESCS.contains(fieldClass)) {

				for (final U enumValue : fieldClass.getEnumConstants()) {

					final Field field = FieldUtils.getEnumField(enumValue);

					final String sqlName = FieldUtils.extractSqlName(field);

					final FieldSpec fieldSpec = new FieldSpec(sqlName);

					FIELD_SPECS.put(enumValue, fieldSpec);
				}
			}
		}

		public String build() {

			// 1. VALIDATE STATE

			checkState(sqlClause != null || notMember != null || !orMembers.isEmpty() || !andMembers.isEmpty(), //
					"sqlClause: %s, orMembers: %s, andMembers: %s", sqlClause, orMembers.size(), andMembers.size());

			checkState(sqlClause == null || notMember == null, //
					"sqlClause: %s, orMembers: %s, andMembers: %s", sqlClause, orMembers.size(), andMembers.size());

			checkState(sqlClause == null && notMember == null || orMembers.isEmpty() && andMembers.isEmpty(), //
					"sqlClause: %s, orMembers: %s, andMembers: %s", sqlClause, orMembers.size(), andMembers.size());

			checkState(orMembers.isEmpty() || andMembers.isEmpty(), //
					"sqlClause: %s, orMembers: %s, andMembers: %s", sqlClause, orMembers.size(), andMembers.size());

			// 2. COMPOSE

			if (sqlClause != null) {

				return sqlClause;

			} else if (notMember != null) {

				final StringBuilder sb = new StringBuilder();

				sb.append("NOT(");

//				final boolean hasMembers = !notMember.andMembers.isEmpty() || !notMember.andMembers.isEmpty();

//				if (hasMembers) {
//					sb.append("(");
//				}

				sb.append(notMember.build());

//				if (hasMembers) {
				sb.append(")");
//				}

				return sb.toString();

			} else if (!orMembers.isEmpty()) {

				final StringBuilder sb = new StringBuilder();

				for (final SqlBuilder<U> orMember : orMembers) {

					if (sb.length() != 0) {

						sb.append(" OR ");
					}

					final boolean hasMembers = true; // !orMember.andMembers.isEmpty() ||
														// !orMember.andMembers.isEmpty();

					if (hasMembers) {
						sb.append("(");
					}

					sb.append(orMember.build());

					if (hasMembers) {
						sb.append(")");
					}
				}

				return sb.toString();

			} else if (!andMembers.isEmpty()) {

				final StringBuilder sb = new StringBuilder();

				for (final SqlBuilder<U> andMember : andMembers) {

					if (sb.length() != 0) {

						sb.append(" AND ");
					}

					final boolean hasMembers = true; // !andMember.andMembers.isEmpty() ||
														// !andMember.andMembers.isEmpty();

					if (hasMembers) {
						sb.append("(");
					}

					sb.append(andMember.build());

					if (hasMembers) {
						sb.append(")");
					}
				}

				return sb.toString();

			} else {

				throw new NotImplementedException("");
			}
		}

		@Override
		public void setTrue() {

			sqlClause = "1";
		}

		@Override
		public void setFalse() {

			sqlClause = "0";
		}

		// ======== STRING COMPARISONS

		@Override
		public void eq(final U field, @Nullable final String refValue) {

			checkNotNull(field, "field");

			if (refValue == null) {

				sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " IS NULL";

			} else {

				sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " = '" + refValue.replace("'", "''") + "'";
			}
		}

		@Override
		public void neq(final U field, @Nullable final String refValue) {

			checkNotNull(field, "field");

			if (refValue == null) {

				sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " IS NOT NULL";

			} else {

				sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " != '" + refValue.replace("'", "''") + "'";
			}
		}

		@Override
		public void contains(final U field, final String refValue) {

			checkNotNull(field, "field");
			checkNotNull(refValue, "refValue");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " LIKE '%" //
					+ refValue //
							.replace("'", "''") //
							.replace("\\", "\\\\") //
							.replace("%", "\\%") //
					+ "%'";
		}

		@Override
		public void doesntContain(final U field, final String refValue) {

			checkNotNull(field, "field");
			checkNotNull(refValue, "refValue");

			sqlClause = "NOT(" + FIELD_SPECS.get(field).sqlName + " LIKE '%" //
					+ refValue //
							.replace("'", "''") //
							.replace("\\", "\\\\") //
							.replace("%", "\\%") //
					+ "%')";
		}

		// ======== BOOLEAN COMPARISONS

		@Override
		public void eq(final U field, final boolean refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " = " + (refValue ? "TRUE" : "FALSE");
		}

		@Override
		public void neq(final U field, final boolean refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " != " + (refValue ? "TRUE" : "FALSE");
		}

		// ======== INT COMPARISONS

		@Override
		public void eq(final U field, final int refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " = " + refValue;
		}

		@Override
		public void neq(final U field, final int refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " != " + refValue;
		}

		@Override
		public void gte(final U field, final int refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " >= " + refValue;
		}

		@Override
		public void gt(final U field, final int refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " > " + refValue;
		}

		@Override
		public void lte(final U field, final int refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " <= " + refValue;
		}

		@Override
		public void lt(final U field, final int refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " < " + refValue;
		}

		// ======== DATETIME COMPARISONS

		private static String formatDateTimeField(final String fieldName, final DateTimePrecision precision) {

			checkNotNull(fieldName, "fieldName");
			checkNotNull(precision, "precision");

			switch (precision) {

			case DAY_OF_MONTH:
				return "DATE_TRUNC('day', " + fieldName + ")";

			case MINUTE:
				return "DATE_TRUNC('minute', " + fieldName + ")";

			case SECOND:
				return "DATE_TRUNC('second', " + fieldName + ")";

			case MILLISECOND:
				return "DATE_TRUNC('milliseconds', " + fieldName + ")";

			default:
				throw new NotImplementedException("precision: " + precision);
			}

		}

		private static String format(final DateTime refValue, final DateTimePrecision precision) {

			checkNotNull(refValue, "refValue");
			checkNotNull(precision, "precision");

			switch (precision) {

			case DAY_OF_MONTH:
				return "TO_TIMESTAMP('" //
						+ refValue.toString(DateTimeFormat.forPattern("yyyy-MM-dd")) //
						+ "', 'YYYY-MM-DD')";

			case MINUTE:
				return "TO_TIMESTAMP('" //
						+ refValue.toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm")) //
						+ "', 'YYYY-MM-DD HH24:MI')";

			case SECOND:
				return "TO_TIMESTAMP('" //
						+ refValue.toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")) //
						+ "', 'YYYY-MM-DD HH24:MI:SS')";

			case MILLISECOND:
				return "TO_TIMESTAMP('" //
						+ refValue.toString(DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS")) //
						+ "', 'YYYY-MM-DD HH24:MI:SS.MS')";

			default:
				throw new NotImplementedException("precision: " + precision);
			}
		}

		@Override
		public void eq(final U field, @Nullable final DateTime refValue, final DateTimePrecision precision) {

			checkNotNull(field, "field");

			if (refValue == null) {

				sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " IS NULL";

			} else {

				sqlClause = formatDateTimeField(FIELD_SPECS.get(field).sqlName, precision)

						+ " = " + format(refValue, precision);
			}
		}

		@Override
		public void neq(final U field, @Nullable final DateTime refValue, final DateTimePrecision precision) {

			checkNotNull(field, "field");

			if (refValue == null) {

				sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " IS NOT NULL";

			} else {

				sqlClause = formatDateTimeField(FIELD_SPECS.get(field).sqlName, precision)

						+ " != " + format(refValue, precision);
			}
		}

		@Override
		public void gte(final U field, final DateTime refValue, final DateTimePrecision precision) {

			checkNotNull(field, "field");

			sqlClause = formatDateTimeField(FIELD_SPECS.get(field).sqlName, precision)

					+ " >= " + format(refValue, precision);
		}

		@Override
		public void gt(final U field, final DateTime refValue, final DateTimePrecision precision) {

			checkNotNull(field, "field");

			sqlClause = formatDateTimeField(FIELD_SPECS.get(field).sqlName, precision)

					+ " > " + format(refValue, precision);
		}

		@Override
		public void lte(final U field, final DateTime refValue, final DateTimePrecision precision) {

			checkNotNull(field, "field");

			sqlClause = formatDateTimeField(FIELD_SPECS.get(field).sqlName, precision)

					+ " <= " + format(refValue, precision);
		}

		@Override
		public void lt(final U field, final DateTime refValue, final DateTimePrecision precision) {

			checkNotNull(field, "field");

			sqlClause = formatDateTimeField(FIELD_SPECS.get(field).sqlName, precision)

					+ " < " + format(refValue, precision);
		}

		// ======== ENUM COMPARISONS

		@Override
		public void eq(final U field, final Enum<?> refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " = '" + refValue.name() + "'";
		}

		@Override
		public void neq(final U field, final Enum<?> refValue) {

			checkNotNull(field, "field");

			sqlClause = prefix + FIELD_SPECS.get(field).sqlName + " != '" + refValue.name() + "'";
		}

		// ======== STRUCTURAL

		@Override
		public SqlBuilder<U> newNotMember() {

			final SqlBuilder<U> notMember = new SqlBuilder<U>(fieldClass, prefix);

			this.notMember = notMember;

			return notMember;
		}

		@Override
		public SqlBuilder<U> newAndMember() {

			final SqlBuilder<U> andMember = new SqlBuilder<U>(fieldClass, prefix);

			andMembers.add(andMember);

			return andMember;
		}

		@Override
		public SqlBuilder<U> newOrMember() {

			final SqlBuilder<U> orMember = new SqlBuilder<U>(fieldClass, prefix);

			orMembers.add(orMember);

			return orMember;
		}
	}
}
