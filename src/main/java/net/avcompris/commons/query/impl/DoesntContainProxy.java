package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.Filtering.Type.DOESNT_CONTAIN;
import static net.avcompris.commons.query.impl.FieldUtils.isStringField;
import static net.avcompris.commons.query.impl.FilteringFieldProxy.extractFieldValue;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;

import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.FilteringHandler;

final class DoesntContainProxy<T extends Filtering<U>, U extends Filtering.Field>
		extends AbstractFilteringFieldProxy<T, U> {

	public DoesntContainProxy(final Class<? extends T> filteringClass, final U field, @Nullable final Object refValue) {

		super(filteringClass, field, "doesnt_contain", refValue);
	}

	@Override
	public boolean match(final Object arg) {

		if (arg == null) {

			return true;
		}

		if (refValue == null) {

			return false;
		}

		final Class<?> argClass = arg.getClass();

		if (String.class.equals(argClass)) {

			return !((String) arg).contains((String) refValue);

		} else if (refValue instanceof String) {

			final String value = extractFieldValue(arg, field, String.class);

			return value == null || !value.contains((String) refValue);
		}

		throw new NotImplementedException("argClass: " + argClass);
	}

	@Override
	public void applyTo(final FilteringHandler<U> handler) {

		if (isStringField(field)) {

			handler.contains(field, (String) refValue);

		} else {

			throw new NotImplementedException("refValue.class: " + refValue.getClass().getName());
		}
	}

	@Override
	public Type getType() {

		return DOESNT_CONTAIN;
	}
}
