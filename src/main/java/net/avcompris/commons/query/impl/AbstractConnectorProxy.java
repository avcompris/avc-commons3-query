package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.lang.reflect.Array;

import javax.annotation.Nullable;

import net.avcompris.commons.query.Filtering;

abstract class AbstractConnectorProxy<T extends Filtering<U>, U extends Filtering.Field>
		extends AbstractFilteringProxy<T, U> implements ConnectorProxy<T, U> {

	private final String connector;
	protected final Filtering<U>[] fs;

	protected AbstractConnectorProxy(final Class<? extends T> filteringClass, final String connector,
			final Filtering<U>[] fs0) {

		super(filteringClass);

		this.connector = checkNotNull(connector, "connector");

		@SuppressWarnings("unchecked")
		final Filtering<U>[] fs = (Filtering<U>[]) Array.newInstance(filteringClass, fs0.length);

		System.arraycopy(fs0, 0, fs, 0, fs0.length);

		this.fs = fs;
	}

	protected AbstractConnectorProxy(final Class<? extends T> filteringClass, final String connector,
			final Filtering<U> f1, final Filtering<U> f2) {

		super(filteringClass);

		this.connector = checkNotNull(connector, "connector");

		@SuppressWarnings("unchecked")
		final Filtering<U>[] fs = (Filtering<U>[]) Array.newInstance(filteringClass, 2);

		fs[0] = f1;
		fs[1] = f2;

		this.fs = fs;
	}

	protected AbstractConnectorProxy(final Class<? extends T> filteringClass, final String connector,
			final Filtering<U> f, final Filtering<U>[] fs0) {

		super(filteringClass);

		this.connector = checkNotNull(connector, "connector");

		@SuppressWarnings("unchecked")
		final Filtering<U>[] fs = (Filtering<U>[]) Array.newInstance(filteringClass, 1 + fs0.length);

		fs[0] = f;

		System.arraycopy(fs0, 0, fs, 1, fs0.length);

		this.fs = fs;
	}

	protected AbstractConnectorProxy(final Class<? extends T> filteringClass, final String connector,
			final Filtering<U>[] fs0, final Filtering<U> f) {

		super(filteringClass);

		this.connector = checkNotNull(connector, "connector");

		@SuppressWarnings("unchecked")
		final Filtering<U>[] fs = (Filtering<U>[]) Array.newInstance(filteringClass, fs0.length + 1);

		System.arraycopy(fs0, 0, fs, 0, fs0.length);

		fs[fs0.length] = f;

		this.fs = fs;
	}

	protected AbstractConnectorProxy(final Class<? extends T> filteringClass, final String connector,
			final Filtering<U>[] fs1, final Filtering<U>[] fs2) {

		super(filteringClass);

		this.connector = checkNotNull(connector, "connector");

		@SuppressWarnings("unchecked")
		final Filtering<U>[] fs = (Filtering<U>[]) Array.newInstance(filteringClass, fs1.length, fs2.length);

		System.arraycopy(fs1, 0, fs, 0, fs1.length);
		System.arraycopy(fs2, 0, fs, fs1.length, fs2.length);

		this.fs = fs;
	}

	@Override
	public final String getConnector() {

		return connector;
	}

	@Override
	public final Filtering<U>[] getFs() {

		return fs;
	}

	@Override
	public final int hashCode() {

		int hashCode = this.getClass().hashCode();

		for (final Filtering<U> f : fs) {

			hashCode += f.hashCode();
		}

		return hashCode;
	}

	@Override
	public final boolean equals(@Nullable final Object arg) {

		if (arg == null || !(arg instanceof FilteringProxy) || !(arg instanceof ConnectorProxy)) {

			return false;
		}

		@SuppressWarnings("unchecked")
		final FilteringProxy<T, U> filteringProxy = (FilteringProxy<T, U>) arg;

		if (!filteringClass.equals(filteringProxy.getFilteringClass())) {

			return false;
		}

		@SuppressWarnings("unchecked")
		final ConnectorProxy<T, U> refProxy = (ConnectorProxy<T, U>) arg;

		if (!connector.equals(refProxy.getConnector())) {
			return false;
		}

		final Filtering<U>[] refFs = refProxy.getFs();

		if (fs.length != refFs.length) {

			return false;
		}

		for (int i = 0; i < fs.length; ++i) {

			if (!fs[i].equals(refFs[i])) {

				return false;
			}
		}

		return true;
	}

	@Override
	public final String toString() {

		final StringBuilder sb = new StringBuilder("(");

		boolean start = true;

		for (final Filtering<U> f : fs) {

			if (start) {

				start = false;

			} else {

				sb.append(' ').append(connector).append(' ');
			}

			sb.append(f);
		}

		return sb.append(")").toString();
	}
}
