package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.Filtering.Type.AND;

import java.util.Arrays;

import net.avcompris.commons.query.And;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.FilteringHandler;

final class AndProxy<T extends Filtering<U>, U extends Filtering.Field> extends AbstractConnectorProxy<T, U>
		implements And<U> {

	public AndProxy(final Class<? extends T> filteringClass, final Filtering<U>[] fs) {

		super(filteringClass, "and", fs);
	}

	public AndProxy(final Class<? extends T> filteringClass, final Filtering<U> f1, final Filtering<U> f2) {

		super(filteringClass, "and", f1, f2);
	}

	public AndProxy(final Class<? extends T> filteringClass, final Filtering<U> f, final Filtering<U>[] fs) {

		super(filteringClass, "and", f, fs);
	}

	public AndProxy(final Class<? extends T> filteringClass, final Filtering<U>[] fs1, final Filtering<U>[] fs2) {

		super(filteringClass, "and", fs1, fs2);
	}

	public AndProxy(final Class<? extends T> filteringClass, final Filtering<U>[] fs, final Filtering<U> f) {

		super(filteringClass, "and", fs, f);
	}

	@Override
	public boolean match(final Object arg) {

		for (final Filtering<U> f : fs) {

			if (!f.match(arg)) {

				return false;
			}
		}

		return true;
	}

	@Override
	public void applyTo(final FilteringHandler<U> handler) {

		for (final Filtering<U> f : fs) {

			final FilteringHandler<U> subHandler = handler.newAndMember();

			f.applyTo(subHandler);
		}
	}

	@Override
	public Type getType() {

		return AND;
	}

	@Override
	public Filtering<U>[] getArgs() {

		return Arrays.copyOf(fs, fs.length);
	}
}
