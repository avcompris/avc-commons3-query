package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Locale.ENGLISH;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.commons.query.Comparison;
import net.avcompris.commons.query.DateTimePrecision;
import net.avcompris.commons.query.Filtering;

abstract class AbstractFilteringFieldProxy<T extends Filtering<U>, U extends Filtering.Field>
		extends AbstractFilteringProxy<T, U> implements FilteringFieldProxy<T, U>, Comparison<U> {

	protected final U field;
	private final String connector;
	@Nullable
	protected final Object refValue;

	protected AbstractFilteringFieldProxy(final Class<? extends T> filteringClass, final U field,
			final String connector, @Nullable final Object refValue) {

		super(filteringClass);

		this.field = checkNotNull(field, "field");
		this.connector = checkNotNull(connector, "connector");
		this.refValue = refValue;
	}

	@Override
	public final U getFilteringField() {

		return field;
	}

	@Override
	@Nullable
	public final Object getRefValue() {

		return refValue;
	}

	@Override
	public final int hashCode() {

		return filteringClass.hashCode() //
				+ field.hashCode() //
				+ (refValue == null ? 0 : refValue.hashCode());
	}

	@Override
	public final boolean equals(@Nullable final Object arg) {

		if (arg == null || !(arg instanceof FilteringFieldProxy)) {

			return false;
		}

		@SuppressWarnings("unchecked")
		final FilteringFieldProxy<T, U> filteringFieldProxy = (FilteringFieldProxy<T, U>) arg;

		if (!filteringClass.equals(filteringFieldProxy.getFilteringClass())) {

			return false;

		} else if (!field.equals(filteringFieldProxy.getFilteringField())) {

			return false;
		}

		final Object argRefValue = filteringFieldProxy.getRefValue();

		if (refValue == null) {

			return argRefValue == null;

		} else if (argRefValue == null) {

			return false;

		} else if (refValue instanceof DateTime && argRefValue instanceof DateTimeArg) {

			final DateTimeArg dateTimeArgRef = (DateTimeArg) argRefValue;

			return dateTimeArgRef.precision == DateTimePrecision.MILLISECOND //
					&& dateTimeArgRef.dateTime.equals(refValue);

		} else if (refValue instanceof Enum<?> && argRefValue instanceof EnumArg) {

			final EnumArg enumArgRef = (EnumArg) argRefValue;

			return enumArgRef.hasEnumValue() //
					&& enumArgRef.enumValue() == refValue;

		} else if (refValue instanceof String && argRefValue instanceof EnumArg) {

			final EnumArg enumArgRef = (EnumArg) argRefValue;

			return enumArgRef.hasEnumValue() //
					? enumArgRef.enumValue().name().equals(refValue) //
					: enumArgRef.s.equals(refValue);
		}

		return refValue.equals(argRefValue);
	}

	@Override
	public final String toString() {

		final StringBuilder sb = new StringBuilder();

		sb.append(field.toString().toLowerCase(ENGLISH));

		sb.append(' ').append(connector).append(' ');

		if (refValue == null) {

			sb.append(refValue);

		} else if (refValue instanceof String) {

			sb.append('"');
			sb.append(((String) refValue) //
					.replace("\\", "\\\\") //
					.replace("\"", "\\\""));
			sb.append('"');

		} else if (refValue instanceof Integer) {

			sb.append(refValue);

		} else if (refValue instanceof Boolean) {

			sb.append(refValue);

		} else if (refValue instanceof DateTime) {

			sb.append('"');
			sb.append(((DateTime) refValue).toString() //
					.replace("\\", "\\\\") //
					.replace("\"", "\\\""));
			sb.append('"');

		} else if (refValue instanceof DateTimeArg) {

			sb.append('"').append(refValue).append('"');

		} else if (refValue instanceof EnumArg) {

			final EnumArg enumArg = (EnumArg) refValue;

			if (enumArg.hasEnumValue()) {

				sb.append(enumArg.enumValue().name());

			} else {

				// sb.append("(UNKNOWN: " + enumArg.s + ")");

				sb.append('"').append(enumArg.s).append('"');
			}

		} else if (refValue instanceof Enum<?>) {

			sb.append(((Enum<?>) refValue).name());

		} else {

			sb.append("UNKNOWN: " + refValue + " (" + refValue.getClass().getName() + ")");
		}

		return sb.toString();
	}

	@Override
	public final U getField() {

		return field;
	}

	@Override
	@Nullable
	public final Object getArg() {

		return refValue;
	}
}
