package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.annotation.Nullable;

import net.avcompris.commons.query.Filtering;

abstract class AbstractFilteringProxy<T extends Filtering<U>, U extends Filtering.Field>
		implements FilteringProxy<T,U>, Filtering<U> {

	protected final Class<? extends T> filteringClass;

	protected AbstractFilteringProxy(final Class<? extends T> filteringClass) {

		this.filteringClass = checkNotNull(filteringClass, "filteringClass");
	}

	@Override
	public final Class<? extends T> getFilteringClass() {

		return filteringClass;
	}

	@Override
	public final FilteringProxy<T,U> getImpl() {

		return this;
	}

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(@Nullable Object arg);

	@Override
	public abstract String toString();
}
