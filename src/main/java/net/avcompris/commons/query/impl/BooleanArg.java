package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import net.avcompris.commons.query.Arg;
import net.avcompris.commons.query.FilterSyntaxException;

final class BooleanArg implements Arg {

	public final boolean b;

	public BooleanArg(final String s) throws FilterSyntaxException {

		checkNotNull(s, "s");

		try {

			this.b = Boolean.parseBoolean(s);

		} catch (final NumberFormatException e) {

			throw new FilterSyntaxException("Cannot parse boolean value: " + s, e);
		}
	}
}
