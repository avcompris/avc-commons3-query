package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.Filtering.Type.EQ;
import static net.avcompris.commons.query.impl.FieldUtils.isBooleanField;
import static net.avcompris.commons.query.impl.FieldUtils.isDateTimeField;
import static net.avcompris.commons.query.impl.FieldUtils.isEnumField;
import static net.avcompris.commons.query.impl.FieldUtils.isIntField;
import static net.avcompris.commons.query.impl.FieldUtils.isStringField;
import static net.avcompris.commons.query.impl.FilteringFieldProxy.extractFieldValue;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;

import net.avcompris.commons.query.DateTimePrecision;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.FilteringHandler;

final class EqProxy<T extends Filtering<U>, U extends Filtering.Field> extends AbstractFilteringFieldProxy<T, U> {

	public EqProxy(final Class<? extends T> filteringClass, final U field, @Nullable final Object refValue) {

		super(filteringClass, field, "eq", refValue);
	}

	@Override
	public boolean match(final Object arg) {

		if (arg == null) {

			return refValue == null;
		}

		if (refValue == null) {

			return false;
		}

		final boolean isEnum = FieldUtils.isEnumField(field);

		final Class<?> argClass = arg.getClass();

		if (String.class.equals(argClass)) {

			return ((String) arg).equals(refValue);

		} else if (Integer.class.equals(argClass)) {

			return ((Integer) arg).equals(refValue);

		} else if (Boolean.class.equals(argClass)) {

			return ((Boolean) arg).equals(refValue);

		} else if (argClass.isPrimitive()) {

			throw new NotImplementedException("argClass: " + argClass);

		} else if (DateTime.class.equals(argClass)) {

			return ((DateTime) arg).equals(refValue);

		} else if (refValue instanceof String) {

			if (isEnum) {

				final Object value = extractFieldValue(arg, field, Enum.class);

				if (value == null) {

					return false;

				} else if (value instanceof Enum<?>) {

					return ((Enum<?>) value).name().equals(refValue);

				} else {

					return refValue.equals((String) value);
				}

			} else {

				final String value = extractFieldValue(arg, field, String.class);

				return refValue.equals(value);
			}

		} else if (refValue instanceof Integer) {

			final int value = extractFieldValue(arg, field, int.class);

			return value == (int) refValue;

		} else if (refValue instanceof Boolean) {

			final boolean value = extractFieldValue(arg, field, boolean.class);

			return value == (boolean) refValue;

		} else if (refValue instanceof Enum) {

			final Object value = extractFieldValue(arg, field, Enum.class);

			if (value == null) {

				return false;

			} else if (value instanceof Enum<?>) {

				return value == (Enum<?>) refValue;

			} else {

				return ((Enum<?>) refValue).name().contentEquals((String) value);
			}

		} else if (refValue instanceof DateTimeArg) {

			final DateTime value = extractFieldValue(arg, field, DateTime.class);

			final DateTimeArg dateTimeArg = (DateTimeArg) refValue;

			return dateTimeArg.eq(value);

		} else if (refValue instanceof EnumArg) {

			final EnumArg enumArg = (EnumArg) refValue;

			final Object value = extractFieldValue(arg, field, Enum.class);

			if (value == null) {

				return false;

			} else if (value instanceof Enum<?>) {

				return enumArg.hasEnumValue()

						? value == enumArg.enumValue()

						: false;

			} else {

				return enumArg.hasEnumValue()

						? enumArg.enumValue().name().contentEquals((String) value)

						: enumArg.s.contentEquals((String) value);
			}
		}

		throw new NotImplementedException("argClass: " + argClass);
	}

	@Override
	public void applyTo(final FilteringHandler<U> handler) {

		if (isStringField(field)) {

			handler.eq(field, (String) refValue);

		} else if (isIntField(field)) {

			handler.eq(field, (int) refValue);

		} else if (isBooleanField(field)) {

			handler.eq(field, (boolean) refValue);

		} else if (isDateTimeField(field)) {

			if (refValue == null) {

				handler.eq(field, (DateTime) null, DateTimePrecision.MILLISECOND);

			} else if (refValue instanceof DateTimeArg) {

				final DateTimeArg dateTimeArg = (DateTimeArg) refValue;

				handler.eq(field, dateTimeArg.dateTime, dateTimeArg.precision);

			} else {

				handler.eq(field, (DateTime) refValue, DateTimePrecision.MILLISECOND);
			}

		} else if (isEnumField(field)) {

			if (refValue == null) {

				handler.eq(field, (Enum<?>) null);

			} else if (refValue instanceof EnumArg) {

				final EnumArg enumArg = (EnumArg) refValue;

				if (enumArg.hasEnumValue()) {

					handler.eq(field, enumArg.enumValue());

				} else {

					handler.eq(field, enumArg.s);
				}

			} else {

				handler.eq(field, (Enum<?>) refValue);
			}

		} else {

			throw new NotImplementedException("refValue.class: " + refValue.getClass().getName());
		}
	}

	@Override
	public Type getType() {

		return EQ;
	}
}
