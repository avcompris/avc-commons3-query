package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons.query.impl.FieldUtils.isDateTimeField;
import static net.avcompris.commons.query.impl.FilteringProxy.invocationHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;

import net.avcompris.commons.query.AlwaysFalse;
import net.avcompris.commons.query.AlwaysTrue;
import net.avcompris.commons.query.Arg;
import net.avcompris.commons.query.Comparison;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.Filtering.Field;
import net.avcompris.commons.query.Filterings;

final class FilteringsInvocationHandler<T extends Filtering<U>, U extends Filtering.Field, V extends Filterings<T, U>>
		implements InvocationHandler {

	private final Class<? extends V> filteringsClass;
	private final Class<? extends T> filteringClass;
	private final Class<? extends U> fieldClass;

	public FilteringsInvocationHandler( //
			final Class<? extends V> filteringsClass, //
			final Class<? extends T> filteringClass, //
			final Class<? extends U> fieldClass) {

		this.filteringsClass = checkNotNull(filteringsClass, "filteringsClass");
		this.filteringClass = checkNotNull(filteringClass, "filteringClass");
		this.fieldClass = checkNotNull(fieldClass, "fieldClass");
	}

	@Override
	public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {

		final Class<?> returnType = method.getReturnType();

		// if (!filteringClass.equals(returnType)) {
		if (!returnType.isAssignableFrom(filteringClass)) {
			throw new NotImplementedException("returnType: " + returnType.getName());
		}

		final String methodName = method.getName();

		final boolean alwaysTrue = "alwaysTrue".contentEquals(methodName);
		final boolean alwaysFalse = "alwaysFalse".contentEquals(methodName);

		if (isParseMethod(method)) {

			final String expression = (String) args[0];

			return new Parser<T, U, V>(filteringsClass, filteringClass, fieldClass).parse(expression);

		} else if (alwaysTrue) {

			return proxyAlwaysTrue(filteringClass);

		} else if (alwaysFalse) {

			return proxyAlwaysFalse(filteringClass);

		} else if (isEqMethod(method)) {

			@SuppressWarnings("unchecked")
			final U field = (U) args[0];

			checkArgument(field != null, "field should not be null");

			final Class<?> refValueClass = method.getParameterTypes()[1];

			@Nullable
			final Object refValue = args[1];

			if (String.class.equals(refValueClass)) {

				if (isDateTimeField(field)) {

					if (refValue == null) {

						return proxyEq(filteringClass, field, (DateTime) null);

					} else {

						return proxyEq(filteringClass, field, new DateTimeArg((String) refValue));
					}

				} else {

					return proxyEq(filteringClass, field, (String) refValue);
				}

			} else if (boolean.class.equals(refValueClass)) {

				return proxyEq(filteringClass, field, (boolean) refValue);

			} else if (int.class.equals(refValueClass)) {

				return proxyEq(filteringClass, field, (int) refValue);

			} else if (DateTime.class.equals(refValueClass)) {

				return proxyEq(filteringClass, field, (DateTime) refValue);

			} else if (refValueClass.isEnum()) {

				return proxyEq(filteringClass, field, (Enum<?>) refValue);

			} else if (Enum.class.equals(refValueClass)) {

				return proxyEq(filteringClass, field, (Enum<?>) refValue);

			} else if (Arg.class.equals(refValueClass)) {

				if (refValue == null || (refValue instanceof NullArg)) {

					return proxyEq(filteringClass, field, (String) null);

				} else if (refValue instanceof StringArg) {

					return proxyEq(filteringClass, field, ((StringArg) refValue).s);

				} else if (refValue instanceof IntArg) {

					return proxyEq(filteringClass, field, ((IntArg) refValue).n);

				} else if (refValue instanceof BooleanArg) {

					return proxyEq(filteringClass, field, ((BooleanArg) refValue).b);

				} else if (refValue instanceof DateTimeArg) {

					return proxyEq(filteringClass, field, (DateTimeArg) refValue);

				} else if (refValue instanceof EnumArg) {

					return proxyEq(filteringClass, field, (EnumArg) refValue);
				}
			}

			throw new NotImplementedException("refValueClass: " + refValueClass);

		} else if (isNeqMethod(method)) {

			@SuppressWarnings("unchecked")
			final U field = (U) args[0];

			checkArgument(field != null, "field should not be null");

			final Class<?> refValueClass = method.getParameterTypes()[1];

			@Nullable
			final Object refValue = args[1];

			if (String.class.equals(refValueClass)) {

				if (isDateTimeField(field)) {

					if (refValue == null) {

						return proxyNeq(filteringClass, field, (DateTime) null);

					} else {

						return proxyNeq(filteringClass, field, new DateTimeArg((String) refValue));
					}

				} else {

					return proxyNeq(filteringClass, field, (String) refValue);
				}

			} else if (boolean.class.equals(refValueClass)) {

				return proxyNeq(filteringClass, field, (boolean) refValue);

			} else if (int.class.equals(refValueClass)) {

				return proxyNeq(filteringClass, field, (int) refValue);

			} else if (DateTime.class.equals(refValueClass)) {

				return proxyNeq(filteringClass, field, (DateTime) refValue);

			} else if (refValueClass.isEnum()) {

				return proxyNeq(filteringClass, field, (Enum<?>) refValue);

			} else if (Enum.class.equals(refValueClass)) {

				return proxyNeq(filteringClass, field, (Enum<?>) refValue);

			} else if (Arg.class.equals(refValueClass)) {

				if (refValue == null || (refValue instanceof NullArg)) {

					return proxyNeq(filteringClass, field, (String) null);

				} else if (refValue instanceof StringArg) {

					return proxyNeq(filteringClass, field, ((StringArg) refValue).s);

				} else if (refValue instanceof IntArg) {

					return proxyNeq(filteringClass, field, ((IntArg) refValue).n);

				} else if (refValue instanceof BooleanArg) {

					return proxyNeq(filteringClass, field, ((BooleanArg) refValue).b);

				} else if (refValue instanceof DateTimeArg) {

					return proxyNeq(filteringClass, field, (DateTimeArg) refValue);

				} else if (refValue instanceof EnumArg) {

					return proxyNeq(filteringClass, field, (EnumArg) refValue);
				}
			}

			throw new NotImplementedException("refValueClass: " + refValueClass);

		} else if (isGtMethod(method)) {

			@SuppressWarnings("unchecked")
			final U field = (U) args[0];

			checkArgument(field != null, "field should not be null");

			final Class<?> refValueClass = method.getParameterTypes()[1];

			@Nullable
			final Object refValue = args[1];

			if (int.class.equals(refValueClass)) {

				return proxyGt(filteringClass, field, (int) refValue);

			} else if (DateTime.class.equals(refValueClass)) {

				return proxyGt(filteringClass, field, (DateTime) refValue);

			} else if (Arg.class.equals(refValueClass)) {

				if (refValue instanceof IntArg) {

					return proxyGt(filteringClass, field, ((IntArg) refValue).n);

				} else if (refValue instanceof DateTimeArg) {

					return proxyGt(filteringClass, field, ((DateTimeArg) refValue).dateTime);
				}
			}

			throw new NotImplementedException("refValueClass: " + refValueClass);

		} else if (isGteMethod(method)) {

			@SuppressWarnings("unchecked")
			final U field = (U) args[0];

			checkArgument(field != null, "field should not be null");

			final Class<?> refValueClass = method.getParameterTypes()[1];

			@Nullable
			final Object refValue = args[1];

			if (int.class.equals(refValueClass)) {

				return proxyGte(filteringClass, field, (int) refValue);

			} else if (DateTime.class.equals(refValueClass)) {

				return proxyGte(filteringClass, field, (DateTime) refValue);

			} else if (Arg.class.equals(refValueClass)) {

				if (refValue instanceof IntArg) {

					return proxyGte(filteringClass, field, ((IntArg) refValue).n);

				} else if (refValue instanceof DateTimeArg) {

					return proxyGte(filteringClass, field, ((DateTimeArg) refValue).dateTime);
				}
			}

			throw new NotImplementedException("refValueClass: " + refValueClass);

		} else if (isLtMethod(method)) {

			@SuppressWarnings("unchecked")
			final U field = (U) args[0];

			checkArgument(field != null, "field should not be null");

			final Class<?> refValueClass = method.getParameterTypes()[1];

			@Nullable
			final Object refValue = args[1];

			if (int.class.equals(refValueClass)) {

				return proxyLt(filteringClass, field, (int) refValue);

			} else if (DateTime.class.equals(refValueClass)) {

				return proxyLt(filteringClass, field, (DateTime) refValue);

			} else if (Arg.class.equals(refValueClass)) {

				if (refValue instanceof IntArg) {

					return proxyLt(filteringClass, field, ((IntArg) refValue).n);

				} else if (refValue instanceof DateTimeArg) {

					return proxyLt(filteringClass, field, ((DateTimeArg) refValue).dateTime);
				}
			}

			throw new NotImplementedException("refValueClass: " + refValueClass);

		} else if (isLteMethod(method)) {

			@SuppressWarnings("unchecked")
			final U field = (U) args[0];

			checkArgument(field != null, "field should not be null");

			final Class<?> refValueClass = method.getParameterTypes()[1];

			@Nullable
			final Object refValue = args[1];

			if (int.class.equals(refValueClass)) {

				return proxyLte(filteringClass, field, (int) refValue);

			} else if (DateTime.class.equals(refValueClass)) {

				return proxyLte(filteringClass, field, (DateTime) refValue);

			} else if (Arg.class.equals(refValueClass)) {

				if (refValue instanceof IntArg) {

					return proxyLte(filteringClass, field, ((IntArg) refValue).n);

				} else if (refValue instanceof DateTimeArg) {

					return proxyLte(filteringClass, field, ((DateTimeArg) refValue).dateTime);
				}
			}

			throw new NotImplementedException("refValueClass: " + refValueClass);

		} else if (isContainsMethod(method)) {

			@SuppressWarnings("unchecked")
			final U field = (U) args[0];

			checkArgument(field != null, "field should not be null");

			final Class<?> refValueClass = method.getParameterTypes()[1];

			@Nullable
			final Object refValue = args[1];

			if (String.class.equals(refValueClass)) {

				return proxyContains(filteringClass, field, (String) refValue);

			} else if (Arg.class.equals(refValueClass)) {

				if (refValue == null || (refValue instanceof NullArg)) {

					return proxyContains(filteringClass, field, (String) null);

				} else if (refValue instanceof StringArg) {

					return proxyContains(filteringClass, field, ((StringArg) refValue).s);
				}
			}

			throw new NotImplementedException("refValueClass: " + refValueClass);

		} else if (isDoesntContainMethod(method)) {

			@SuppressWarnings("unchecked")
			final U field = (U) args[0];

			checkArgument(field != null, "field should not be null");

			final Class<?> refValueClass = method.getParameterTypes()[1];

			@Nullable
			final Object refValue = args[1];

			if (String.class.equals(refValueClass)) {

				return proxyDoesntContain(filteringClass, field, (String) refValue);

			} else if (Arg.class.equals(refValueClass)) {

				if (refValue == null || (refValue instanceof NullArg)) {

					return proxyDoesntContain(filteringClass, field, (String) null);

				} else if (refValue instanceof StringArg) {

					return proxyDoesntContain(filteringClass, field, ((StringArg) refValue).s);
				}
			}

			throw new NotImplementedException("refValueClass: " + refValueClass);
		}

		throw new NotImplementedException("method: " + method);
	}

	private boolean isEqMethod(final Method method) {

		checkNotNull(method, "method");

		return "eq".contentEquals(method.getName()) //
				// && filteringClass.equals(method.getReturnType()) //
				&& method.getReturnType().isAssignableFrom(filteringClass) //
				&& method.getParameterCount() == 2;
	}

	private boolean isNeqMethod(final Method method) {

		checkNotNull(method, "method");

		return "neq".contentEquals(method.getName()) //
				// && filteringClass.equals(method.getReturnType()) //
				&& method.getReturnType().isAssignableFrom(filteringClass) //
				&& method.getParameterCount() == 2;
	}

	private boolean isGtMethod(final Method method) {

		checkNotNull(method, "method");

		return "gt".contentEquals(method.getName()) //
				// && filteringClass.equals(method.getReturnType()) //
				&& method.getReturnType().isAssignableFrom(filteringClass) //
				&& method.getParameterCount() == 2;
	}

	private boolean isGteMethod(final Method method) {

		checkNotNull(method, "method");

		return "gte".contentEquals(method.getName()) //
				// && filteringClass.equals(method.getReturnType()) //
				&& method.getReturnType().isAssignableFrom(filteringClass) //
				&& method.getParameterCount() == 2;
	}

	private boolean isLtMethod(final Method method) {

		checkNotNull(method, "method");

		return "lt".contentEquals(method.getName()) //
				// && filteringClass.equals(method.getReturnType()) //
				&& method.getReturnType().isAssignableFrom(filteringClass) //
				&& method.getParameterCount() == 2;
	}

	private boolean isLteMethod(final Method method) {

		checkNotNull(method, "method");

		return "lte".equals(method.getName()) //
				// && filteringClass.equals(method.getReturnType()) //
				&& method.getReturnType().isAssignableFrom(filteringClass) //
				&& method.getParameterCount() == 2;
	}

	private boolean isContainsMethod(final Method method) {

		checkNotNull(method, "method");

		return "contains".contentEquals(method.getName()) //
				// && filteringClass.equals(method.getReturnType()) //
				&& method.getReturnType().isAssignableFrom(filteringClass) //
				&& method.getParameterCount() == 2;
	}

	private boolean isDoesntContainMethod(final Method method) {

		checkNotNull(method, "method");

		return "doesntContain".contentEquals(method.getName()) //
				// && filteringClass.equals(method.getReturnType()) //
				&& method.getReturnType().isAssignableFrom(filteringClass) //
				&& method.getParameterCount() == 2;
	}

	private boolean isParseMethod(final Method method) {

		checkNotNull(method, "method");

		return "parse".contentEquals(method.getName()) //
				// && filteringClass.equals(method.getReturnType()) //
				&& method.getReturnType().isAssignableFrom(filteringClass) //
				&& method.getParameterCount() == 1 //
				&& String.class.equals(method.getParameterTypes()[0]);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyAlwaysTrue(final Class<T> filteringClass) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
						FilteringProxy.class, //
						filteringClass, //
						AlwaysTrue.class, //
				}, invocationHandler(new AlwaysTrueProxy<T, U>(filteringClass)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyAlwaysFalse(final Class<T> filteringClass) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
						FilteringProxy.class, //
						filteringClass, //
						AlwaysFalse.class, //
				}, invocationHandler(new AlwaysFalseProxy<T, U>(filteringClass)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyEq(final Class<T> filteringClass, final U field,
			@Nullable final String refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
						FilteringFieldProxy.class, //
						filteringClass, //
						Comparison.class, //
				}, invocationHandler(new EqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyNeq(final Class<T> filteringClass, final U field,
			@Nullable final String refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
						FilteringFieldProxy.class, //
						filteringClass, //
						Comparison.class, //
				}, invocationHandler(new NeqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyContains(final Class<T> filteringClass,
			final U field, @Nullable final String refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new ContainsProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyDoesntContain(final Class<T> filteringClass,
			final U field, @Nullable final String refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new DoesntContainProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyEq(final Class<T> filteringClass, final U field,
			final boolean refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new EqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyEq(final Class<T> filteringClass, final U field,
			final int refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new EqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyEq(final Class<T> filteringClass, final U field,
			final Enum<?> refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new EqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyGt(final Class<T> filteringClass, final U field,
			final int refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new GtProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyGte(final Class<T> filteringClass, final U field,
			final int refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, // 
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new GteProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyLt(final Class<T> filteringClass, final U field,
			final int refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new LtProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyLte(final Class<T> filteringClass, final U field,
			final int refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new LteProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyEq(final Class<T> filteringClass, final U field,
			final DateTime refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new EqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyNeq(final Class<T> filteringClass, final U field,
			final DateTime refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new NeqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyGt(final Class<T> filteringClass, final U field,
			final DateTime refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new GtProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyGte(final Class<T> filteringClass, final U field,
			final DateTime refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new GteProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyLt(final Class<T> filteringClass, final U field,
			final DateTime refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new LtProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyLte(final Class<T> filteringClass, final U field,
			final DateTime refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new LteProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyEq(final Class<T> filteringClass, final U field,
			@Nullable final Arg refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new EqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyNeq(final Class<T> filteringClass, final U field,
			@Nullable final Arg refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new NeqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyNeq(final Class<T> filteringClass, final U field,
			final boolean refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new NeqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyNeq(final Class<T> filteringClass, final U field,
			final int refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { // 
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new NeqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyNeq(final Class<T> filteringClass, final U field,
			final Enum<?> refValue) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringFieldProxy.class, //
			filteringClass, //
			Comparison.class, //
		},
				invocationHandler(new NeqProxy<T, U>(filteringClass, field, refValue)));

		return filteringClass.cast(proxy);
	}
}
