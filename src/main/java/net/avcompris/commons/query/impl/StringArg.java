package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import net.avcompris.commons.query.Arg;
import net.avcompris.commons.query.FilterSyntaxException;

final class StringArg implements Arg {

	public final String s;

	public StringArg(final String s) throws FilterSyntaxException {

		this.s = checkNotNull(s, "s");
	}
}
