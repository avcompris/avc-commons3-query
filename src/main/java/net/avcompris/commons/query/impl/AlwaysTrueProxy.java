package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.Filtering.Type.TRUE;

import net.avcompris.commons.query.AlwaysTrue;
import net.avcompris.commons.query.Filtering;

final class AlwaysTrueProxy<T extends Filtering<U>, U extends Filtering.Field> extends AbstractAlwaysXxxProxy<T, U>
		implements AlwaysTrue<U> {

	public AlwaysTrueProxy(final Class<? extends T> filteringClass) {

		super(filteringClass, true);
	}

	@Override
	public Type getType() {

		return TRUE;
	}
}
