package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import net.avcompris.commons.query.Arg;
import net.avcompris.commons.query.FilterSyntaxException;

final class IntArg implements Arg {

	public final int n;

	public IntArg(final String s) throws FilterSyntaxException {

		checkNotNull(s, "s");

		try {

			this.n = Integer.parseInt(s);

		} catch (final NumberFormatException e) {

			throw new FilterSyntaxException("Cannot parse int value: " + s, e);
		}
	}
}
