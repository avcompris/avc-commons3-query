package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.Filtering.Type.FALSE;

import net.avcompris.commons.query.AlwaysFalse;
import net.avcompris.commons.query.Filtering;

final class AlwaysFalseProxy<T extends Filtering<U>, U extends Filtering.Field> extends AbstractAlwaysXxxProxy<T, U>
		implements AlwaysFalse<U> {

	public AlwaysFalseProxy(final Class<? extends T> filteringClass) {

		super(filteringClass, false);
	}

	@Override
	public Type getType() {

		return FALSE;
	}
}
