package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.Filtering.Type.NEQ;
import static net.avcompris.commons.query.impl.FieldUtils.isBooleanField;
import static net.avcompris.commons.query.impl.FieldUtils.isDateTimeField;
import static net.avcompris.commons.query.impl.FieldUtils.isEnumField;
import static net.avcompris.commons.query.impl.FieldUtils.isIntField;
import static net.avcompris.commons.query.impl.FieldUtils.isStringField;
import static net.avcompris.commons.query.impl.FilteringFieldProxy.extractFieldValue;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;

import net.avcompris.commons.query.DateTimePrecision;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.FilteringHandler;

final class NeqProxy<T extends Filtering<U>, U extends Filtering.Field> extends AbstractFilteringFieldProxy<T, U> {

	public NeqProxy(final Class<? extends T> filteringClass, final U field, @Nullable final Object refValue) {

		super(filteringClass, field, "neq", refValue);
	}

	@Override
	public boolean match(final Object arg) {

		if (arg == null) {

			return refValue != null;
		}

		if (refValue == null) {

			return true;
		}

		final Class<?> argClass = arg.getClass();

		if (String.class.equals(argClass)) {

			return !((String) arg).equals(refValue);

		} else if (Integer.class.equals(argClass)) {

			return !((Integer) arg).equals(refValue);

		} else if (Boolean.class.equals(argClass)) {

			return !((Boolean) arg).equals(refValue);

		} else if (argClass.isPrimitive()) {

			throw new NotImplementedException("argClass: " + argClass);

		} else if (DateTime.class.equals(argClass)) {

			return !((DateTime) arg).equals(refValue);

		} else if (refValue instanceof String) {

			final String value = extractFieldValue(arg, field, String.class);

			return !refValue.equals(value);

		} else if (refValue instanceof Integer) {

			final int value = extractFieldValue(arg, field, int.class);

			return value != (int) refValue;

		} else if (refValue instanceof Boolean) {

			final boolean value = extractFieldValue(arg, field, boolean.class);

			return value != (boolean) refValue;

		} else if (refValue instanceof DateTimeArg) {

			final DateTime value = extractFieldValue(arg, field, DateTime.class);

			final DateTimeArg dateTimeArg = (DateTimeArg) refValue;

			return dateTimeArg.neq(value);

		} else if (refValue instanceof EnumArg) {

			final EnumArg enumArg = (EnumArg) refValue;

			final Object value = extractFieldValue(arg, field, Enum.class);

			if (value == null) {

				return true;

			} else if (value instanceof Enum<?>) {

				return enumArg.hasEnumValue()

						? value != enumArg.enumValue()

						: true;

			} else {

				return enumArg.hasEnumValue()

						? !enumArg.enumValue().name().equals((String) value)

						: !enumArg.s.equals((String) value);
			}
		}

		throw new NotImplementedException("argClass: " + argClass);
	}

	@Override
	public void applyTo(final FilteringHandler<U> handler) {

		if (isStringField(field)) {

			handler.neq(field, (String) refValue);

		} else if (isIntField(field)) {

			handler.neq(field, (int) refValue);

		} else if (isBooleanField(field)) {

			handler.neq(field, (boolean) refValue);

		} else if (isDateTimeField(field)) {

			if (refValue instanceof DateTimeArg) {

				final DateTimeArg dateTimeArg = (DateTimeArg) refValue;

				handler.neq(field, dateTimeArg.dateTime, dateTimeArg.precision);

			} else {

				handler.neq(field, (DateTime) refValue, DateTimePrecision.MILLISECOND);
			}

		} else if (isEnumField(field)) {

			if (refValue instanceof EnumArg) {

				final EnumArg enumArg = (EnumArg) refValue;

				if (enumArg.hasEnumValue()) {

					handler.neq(field, enumArg.enumValue());

				} else {

					handler.neq(field, enumArg.s);
				}

			} else {

				handler.neq(field, (Enum<?>) refValue);
			}

		} else {

			throw new NotImplementedException("refValue.class: " + refValue.getClass().getName());
		}
	}

	@Override
	public Type getType() {

		return NEQ;
	}
}
