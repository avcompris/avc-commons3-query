package net.avcompris.commons.query.impl;

import static org.apache.commons.lang3.StringUtils.uncapitalize;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import javax.annotation.Nullable;

import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.Filtering.Field;

interface FilteringProxy<T extends Filtering<U>, U extends Field> {

	Class<? extends T> getFilteringClass();

	// Class<? extends FilteringProxy> getProxyClass();

	FilteringProxy<T, U> getImpl();

	/*
	 * static boolean isGetFilteringClassMethod(final Method method) {
	 * 
	 * checkNotNull(method, "method");
	 * 
	 * return "getFilteringClass".equals(method.getName()) // &&
	 * Class.class.equals(method.getReturnType()) // && method.getParameterCount()
	 * == 0; } /* static boolean isMatchMethod(final Method method) {
	 * 
	 * checkNotNull(method, "method");
	 * 
	 * return "match".equals(method.getName()) // &&
	 * boolean.class.equals(method.getReturnType()) // && method.getParameterCount()
	 * == 1; }
	 * 
	 * static boolean isApplyToMethod(final Method method) {
	 * 
	 * checkNotNull(method, "method");
	 * 
	 * return "applyTo".equals(method.getName()) // &&
	 * void.class.equals(method.getReturnType()) // && method.getParameterCount() ==
	 * 1; }
	 */
	@Nullable
	static String extractPropertyName(final String methodName, final Class<?> fieldClass) {

		if (methodName.startsWith("is") && boolean.class.equals(fieldClass)) {

			return uncapitalize(methodName.substring(2));

		} else if (methodName.startsWith("has") && boolean.class.equals(fieldClass)) {

			return uncapitalize(methodName.substring(3));

		} else if (methodName.startsWith("get")) {

			return uncapitalize(methodName.substring(3));
		}

		return null;
	}

	static <T extends Filtering<U>, U extends Field> InvocationHandler invocationHandler(
			final FilteringProxy<T, U> myProxy) {

		return new InvocationHandler() {

			@Override
			public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {

				return method.invoke(myProxy, args);
			}
		};
	}
}