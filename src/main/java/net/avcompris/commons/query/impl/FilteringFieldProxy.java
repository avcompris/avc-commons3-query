package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons.query.impl.FilteringProxy.extractPropertyName;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.avcompris.commons.query.FilterSemanticsException;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.Filtering.Field;

interface FilteringFieldProxy<T extends Filtering<U>, U extends Field> extends FilteringProxy<T, U> {

	U getFilteringField();

	Object getRefValue();

	static <U> U extractFieldValue(final Object arg, final Field field, final Class<U> fieldClass) {

		checkNotNull(arg, "arg");
		checkNotNull(field, "field");
		checkNotNull(fieldClass, "fieldClass");

		final String fieldPropertyName = FieldUtils.extractPropertyName(field);

		final Class<?> argClass = arg.getClass();

		final boolean isEnum = FieldUtils.isEnumField(field);

		for (final Method method : argClass.getMethods()) {

			if (method.getParameterCount() != 0) {
				continue;
			}

			final Class<?> returnType = method.getReturnType();

			if (!(fieldClass.isAssignableFrom(returnType) //
					|| returnType.isEnum() && isEnum //
					|| String.class.equals(returnType) && isEnum)) {
				continue;
			}

			final String methodName = method.getName();

			final String propertyName = extractPropertyName(methodName, fieldClass);

			if (propertyName == null || !fieldPropertyName.contentEquals(propertyName)) {
				continue;
			}

			final Object fieldValue;

			method.setAccessible(true);

			try {

				fieldValue = method.invoke(arg);

			} catch (final IllegalAccessException e) {

				throw new RuntimeException(e);

			} catch (final InvocationTargetException e) {

				throw new RuntimeException(e);
			}

			@SuppressWarnings("unchecked")
			final U typedValue = (U) fieldValue;

			return typedValue;
		}

		throw new FilterSemanticsException("argClass: " + arg.getClass().getName() + ", field: " + field);
	}
}
