package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons.query.Filtering.Type.NOT;

import javax.annotation.Nullable;

import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.FilteringHandler;
import net.avcompris.commons.query.Not;

final class NotProxy<T extends Filtering<U>, U extends Filtering.Field> extends AbstractFilteringProxy<T, U>
		implements Not<U> {

	private final Filtering<U> f;

	public NotProxy(final Class<? extends T> filteringClass, final Filtering<U> f) {

		super(filteringClass);

		this.f = checkNotNull(f, "f");
	}

	@Override
	public boolean match(final Object arg) {

		return !f.match(arg);
	}

	@Override
	public void applyTo(final FilteringHandler<U> handler) {

		final FilteringHandler<U> subHandler = handler.newNotMember();

		f.applyTo(subHandler);
	}

	@Override
	public int hashCode() {

		return 10_000 - 2 * f.hashCode();
	}

	@Override
	public boolean equals(@Nullable final Object arg) {

		if (arg == null || !(arg instanceof FilteringProxy)) {

			return false;
		}

		@SuppressWarnings("unchecked")
		final FilteringProxy<T, U> filteringProxy = (FilteringProxy<T, U>) arg;

		if (!filteringClass.equals(filteringProxy.getFilteringClass())) {

			return false;
		}

		final NotProxy<T, U> refProxy = (NotProxy<T, U>) filteringProxy.getImpl();

		return f.equals(refProxy.f);
	}

	@Override
	public String toString() {

		return "not(" + f + ")";
	}

	@Override
	public Type getType() {

		return NOT;
	}

	@Override
	public Filtering<U> getArg() {

		return f;
	}
}
