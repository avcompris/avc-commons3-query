package net.avcompris.commons.query.impl;

import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.Filtering.Field;

interface ConnectorProxy<T extends Filtering<U>, U extends Field> extends FilteringProxy<T, U> {

	String getConnector();

	Filtering<U>[] getFs();
}