package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.joda.time.DateTimeZone.UTC;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.format.DateTimeFormat;

import net.avcompris.commons.query.Arg;
import net.avcompris.commons.query.DateTimePrecision;
import net.avcompris.commons.query.FilterSyntaxException;

final class DateTimeArg implements Arg {

	public final DateTime dateTime;
	public final DateTimePrecision precision;

	public DateTimeArg(final String s) throws FilterSyntaxException {

		try {

			switch (s.length()) {

			case 10:

				this.dateTime = DateTimeFormat.forPattern("yyyy-MM-dd").withZoneUTC() //
						.parseDateTime(s).withZone(UTC);

				this.precision = DateTimePrecision.DAY_OF_MONTH;

				break;

			case 16:

				this.dateTime = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm").withZoneUTC() //
						.parseDateTime(s).withZone(UTC);

				this.precision = DateTimePrecision.MINUTE;

				break;

			case 19:

				this.dateTime = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").withZoneUTC() //
						.parseDateTime(s).withZone(UTC);

				this.precision = DateTimePrecision.SECOND;

				break;

			case 23:

				this.dateTime = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS").withZoneUTC() //
						.parseDateTime(s).withZone(UTC);

				this.precision = DateTimePrecision.MILLISECOND;

				break;

			default:

				if (s.length() > 23) {

					this.dateTime = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ").withZoneUTC() //
							.parseDateTime(s).withZone(UTC);

					this.precision = DateTimePrecision.MILLISECOND;

				} else {

					throw new IllegalArgumentException("Unknown format: " + s);
				}

				break;
			}

		} catch (final IllegalFieldValueException e) {

			throw new FilterSyntaxException("Cannot parse date: " + s, e);
		}
	}

	private DateTimeArg(final DateTime dateTime, final DateTimePrecision precision) {

		this.dateTime = checkNotNull(dateTime, "dateTime");
		this.precision = checkNotNull(precision, "precision");
	}

	@Override
	public String toString() {

		switch (precision) {

		case DAY_OF_MONTH:
			return dateTime.toString(DateTimeFormat.forPattern("yyyy-MM-dd"));

		case MINUTE:
			return dateTime.toString(DateTimeFormat.forPattern("yyyy-MM-dd'T':HH:mm"));

		case SECOND:
			return dateTime.toString(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss"));

		case MILLISECOND:
			return dateTime.toString(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));

		default:
			throw new NotImplementedException("precision: " + precision);
		}
	}

	@Override
	public int hashCode() {

		return dateTime.hashCode() + precision.hashCode();
	}

	@Override
	public boolean equals(@Nullable final Object arg) {

		if (arg == null || !(arg instanceof DateTimeArg)) {
			return false;
		}

		return toString().equals(arg.toString());
	}

	public boolean eq(final DateTime dateTime) {

		return this.equals(new DateTimeArg(dateTime, precision));
	}

	public boolean neq(final DateTime dateTime) {

		return !this.equals(new DateTimeArg(dateTime, precision));
	}

	public boolean gt(final DateTime dateTime) {

		return this.toString().compareTo( //
				new DateTimeArg(dateTime, precision).toString()) > 0;
	}

	public boolean gte(final DateTime dateTime) {

		return this.toString().compareTo( //
				new DateTimeArg(dateTime, precision).toString()) >= 0;
	}

	public boolean lt(final DateTime dateTime) {

		return this.toString().compareTo( //
				new DateTimeArg(dateTime, precision).toString()) < 0;
	}

	public boolean lte(final DateTime dateTime) {

		return this.toString().compareTo( //
				new DateTimeArg(dateTime, precision).toString()) <= 0;
	}
}
