package net.avcompris.commons.query.impl;

import net.avcompris.commons.query.Arg;

final class NullArg implements Arg {

	public static final Arg INSTANCE = new NullArg();
}
