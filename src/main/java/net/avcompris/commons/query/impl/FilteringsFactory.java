package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons.query.impl.FieldUtils.extractFieldClass;
import static net.avcompris.commons.query.impl.FilteringProxy.invocationHandler;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.annotation.Nullable;

import net.avcompris.commons.query.And;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.Filtering.Field;
import net.avcompris.commons.query.Filterings;
import net.avcompris.commons.query.Not;
import net.avcompris.commons.query.Or;

public abstract class FilteringsFactory {

	public static boolean match(@Nullable final Object arg, final Filtering<? extends Field> filtering) {

		checkNotNull(filtering, "filtering");

		return filtering.match(arg);
	}

	private static <T extends Filtering<U>, U extends Field, V extends Filterings<T, U>> Class<? extends T> extractFilteringClass(
			final Class<V> filteringsClass) {

		checkNotNull(filteringsClass, "filteringsClass");

		for (final Method method : filteringsClass.getDeclaredMethods()) {

			final Class<?> returnType = method.getReturnType();

			if (returnType == null //
					|| !Filtering.class.isAssignableFrom(returnType) //
					|| Filtering.class.equals(returnType)) {
				continue;
			}

			@SuppressWarnings("unchecked")
			final Class<? extends T> filteringClass = (Class<? extends T>) returnType;

			return filteringClass;
		}

		throw new IllegalStateException(
				"Cannot extract filteringClass from filteringsClass: " + filteringsClass.getName());
	}

	private static <T extends Filtering<U>, U extends Field> Class<? extends T> extractFilteringClass(
			final Filtering<U> filtering) {

		checkNotNull(filtering, "filtering");

		checkArgument(FilteringProxy.class.isInstance(filtering), //
				"filtering should be an instance of: %s, but was: %s", //
				FilteringProxy.class.getSimpleName(), //
				filtering.getClass().getName());

		@SuppressWarnings("unchecked")
		final FilteringProxy<T, U> filteringProxy = (FilteringProxy<T, U>) filtering;

		return filteringProxy.getFilteringClass();
	}

	public static <T extends Filtering<U>, U extends Field, V extends Filterings<T, U>> V instantiate(
			final Class<V> filteringsClass) {

		checkNotNull(filteringsClass, "filteringsClass");

		checkArgument(filteringsClass.isInterface(), //
				"filteringsClass should be an interface: %s", filteringsClass.getName());

		final Class<? extends T> filteringClass = extractFilteringClass(filteringsClass);
		final Class<? extends U> fieldClass = extractFieldClass(filteringClass);

		final ClassLoader classLoader = filteringsClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { filteringsClass }, //
				new FilteringsInvocationHandler<T, U, V>(filteringsClass, filteringClass, fieldClass));

		return filteringsClass.cast(proxy);
	}

	static <T extends Filtering<U>, U extends Field> T proxy(final Class<?>[] interfaces,
			final FilteringProxy<T, U> f) {

		final Class<? extends T> filteringClass = f.getFilteringClass();

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				interfaces, //
				invocationHandler(f));

		return filteringClass.cast(proxy);
	}

	public static <T extends Filtering<U>, U extends Field> T and(final T f0, final T f1,
			@SuppressWarnings("unchecked") final T... fn) {

		checkNotNull(f0, "f0");
		checkNotNull(f1, "f1");
		checkNotNull(fn, "fn");

		final Class<? extends T> filteringClass = extractFilteringClass(f0);

		@SuppressWarnings("unchecked")
		final Filtering<U>[] fs = new Filtering[fn.length + 2];

		fs[0] = f0;
		fs[1] = f1;

		System.arraycopy(fn, 0, fs, 2, fn.length);

		return proxyAnd(filteringClass, fs);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyAnd(final Class<T> filteringClass,
			final Filtering<U>[] fs) {

		return proxy(new Class<?>[] { //
				ConnectorProxy.class, //
				filteringClass, //
				And.class, //
		}, new AndProxy<T, U>(filteringClass, fs));
	}

	public static <T extends Filtering<U>, U extends Field> T or(final T f0, final T f1,
			@SuppressWarnings("unchecked") final T... fn) {

		checkNotNull(f0, "f0");
		checkNotNull(f1, "f1");
		checkNotNull(fn, "fn");

		final Class<? extends T> filteringClass = extractFilteringClass(f0);

		@SuppressWarnings("unchecked")
		final Filtering<U>[] fs = new Filtering[fn.length + 2];

		fs[0] = f0;
		fs[1] = f1;

		System.arraycopy(fn, 0, fs, 2, fn.length);

		return proxyOr(filteringClass, fs);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyOr(final Class<T> filteringClass,
			final Filtering<U>[] fs) {

		return proxy(new Class<?>[] { //
				ConnectorProxy.class, //
				filteringClass, //
				Or.class, //
		}, new OrProxy<T, U>(filteringClass, fs));
	}

	public static <T extends Filtering<U>, U extends Field> T not(final Filtering<U> f) {

		checkNotNull(f, "f");

		final Class<? extends T> filteringClass = extractFilteringClass(f);

		return proxyNot(filteringClass, f);
	}

	private static <T extends Filtering<U>, U extends Field> T proxyNot(final Class<T> filteringClass,
			final Filtering<U> f) {

		final ClassLoader classLoader = filteringClass.getClassLoader();

		final Object proxy = Proxy.newProxyInstance(classLoader, //
				new Class<?>[] { //
			FilteringProxy.class, //
			filteringClass, //
			Not.class, //
		},
				invocationHandler(new NotProxy<T, U>(filteringClass, f)));

		return filteringClass.cast(proxy);

	}
}
