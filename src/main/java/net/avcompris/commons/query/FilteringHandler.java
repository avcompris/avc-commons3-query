package net.avcompris.commons.query;

import org.joda.time.DateTime;

public interface FilteringHandler<U extends Filtering.Field> {

	void setTrue();

	void setFalse();

	// ======== STRING COMPARISONS

	void eq(U field, String refValue);

	void neq(U field, String refValue);

	void contains(U field, String refValue);

	void doesntContain(U field, String refValue);

	// ======== BOOLEAN COMPARISONS

	void eq(U field, boolean refValue);

	void neq(U field, boolean refValue);

	// ======== INT COMPARISONS

	void eq(U field, int refValue);

	void neq(U field, int refValue);

	void gte(U field, int refValue);

	void gt(U field, int refValue);

	void lte(U field, int refValue);

	void lt(U field, int refValue);

	// ======== DATETIME COMPARISONS

//	void eq(U field, DateTime refValue);

	void eq(U field, DateTime refValue, DateTimePrecision precision);

//	void neq(U field, DateTime refValue);

	void neq(U field, DateTime refValue, DateTimePrecision precision);

//	void gte(U field, DateTime refValue);

	void gte(U field, DateTime refValue, DateTimePrecision precision);

//	void gt(U field, DateTime refValue);

	void gt(U field, DateTime refValue, DateTimePrecision precision);

//	void lte(U field, DateTime refValue);

	void lte(U field, DateTime refValue, DateTimePrecision precision);

//	void lt(U field, DateTime refValue);

	void lt(U field, DateTime refValue, DateTimePrecision precision);

	// ======== ENUM COMPARISONS

	void eq(U field, Enum<?> refValue);

	void neq(U field, Enum<?> refValue);

	// ======== STRUCTURAL

	FilteringHandler<U> newNotMember();

	FilteringHandler<U> newAndMember();

	FilteringHandler<U> newOrMember();
}
