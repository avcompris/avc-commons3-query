package net.avcompris.commons.query;

/**
 * Interface for "<code>OR</code>"-typed Filterings.
 */
public interface Or<U extends Filtering.Field> extends Filtering<U> {

	Filtering<U>[] getArgs();
}
