package net.avcompris.commons.query;

import javax.annotation.Nullable;

public final class FilterSemanticsException extends RuntimeException {

	private static final long serialVersionUID = 2712952620800037087L;

	public FilterSemanticsException(@Nullable final String message) {

		super(message);
	}
}
