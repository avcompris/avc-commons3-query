package net.avcompris.commons.query;

/**
 * Interface for "<code>AND</code>"-typed Filterings.
 */
public interface And<U extends Filtering.Field> extends Filtering<U> {

	Filtering<U>[] getArgs();
}
