package net.avcompris.commons.query;

/**
 * Interface for "<code>TRUE</code>"-typed Filterings.
 */
public interface AlwaysTrue<U extends Filtering.Field> extends Filtering<U> {

}
