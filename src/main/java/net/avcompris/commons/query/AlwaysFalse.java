package net.avcompris.commons.query;

/**
 * Interface for "<code>FALSE</code>"-typed Filterings.
 */
public interface AlwaysFalse<U extends Filtering.Field> extends Filtering<U> {

}
