package net.avcompris.commons.query;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.annotation.Nullable;

public interface Filtering<U extends Filtering.Field> {

	interface Field {

		@Target(FIELD)
		@Retention(RUNTIME)
		@interface Spec {

			Class<?> type();

			String[] alias() default {};

			String propertyName() default "";

			String sqlName() default "";
		}
	}

	boolean match(@Nullable Object arg);

	void applyTo(FilteringHandler<U> handler);

	Type getType();

	enum Type {

		TRUE, FALSE,

		NOT,

		AND, OR,

		EQ, NEQ, CONTAINS, DOESNT_CONTAIN, LT, LTE, GT, GTE,
	}
}
