package net.avcompris.commons.query;

public enum DateTimePrecision {

	DAY_OF_MONTH, //
	MINUTE, //
	SECOND, //
	MILLISECOND,
}
