package net.avcompris.commons.query;

/**
 * Interface for "<code>NOT</code>"-typed Filterings.
 */
public interface Not<U extends Filtering.Field> extends Filtering<U> {

	Filtering<U> getArg();
}
