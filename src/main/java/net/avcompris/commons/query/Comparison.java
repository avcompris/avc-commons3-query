package net.avcompris.commons.query;

import javax.annotation.Nullable;

/**
 * Interface for "<code>EQ</code>", "<code>NEQ</code>", "<code>LT</code>", etc.-typed
 * Filterings.
 */
public interface Comparison<U extends Filtering.Field> extends Filtering<U> {

	U getField();

	@Nullable
	Object getArg();
}
