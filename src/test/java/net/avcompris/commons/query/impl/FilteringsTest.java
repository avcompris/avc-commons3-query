package net.avcompris.commons.query.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons.query.Filtering.Type.AND;
import static net.avcompris.commons.query.Filtering.Type.EQ;
import static net.avcompris.commons.query.Filtering.Type.FALSE;
import static net.avcompris.commons.query.Filtering.Type.NOT;
import static net.avcompris.commons.query.Filtering.Type.OR;
import static net.avcompris.commons.query.Filtering.Type.TRUE;
import static net.avcompris.commons.query.impl.FilteringsFactory.and;
import static net.avcompris.commons.query.impl.FilteringsFactory.instantiate;
import static net.avcompris.commons.query.impl.FilteringsFactory.match;
import static net.avcompris.commons.query.impl.FilteringsFactory.not;
import static net.avcompris.commons.query.impl.FilteringsFactory.or;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import net.avcompris.commons.query.AlwaysFalse;
import net.avcompris.commons.query.AlwaysTrue;
import net.avcompris.commons.query.And;
import net.avcompris.commons.query.Comparison;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.Filterings;
import net.avcompris.commons.query.Not;
import net.avcompris.commons.query.Or;

public class FilteringsTest {

	private interface EasyBooleanFiltering extends Filtering<EasyBooleanFiltering.Field> {

		enum Field implements Filtering.Field {

		}
	}

	private interface EasyBooleanFilterings extends Filterings<EasyBooleanFiltering, EasyBooleanFiltering.Field> {

		@Override
		EasyBooleanFiltering alwaysTrue();

		@Override
		EasyBooleanFiltering alwaysFalse();
	}

	@Test
	public void testAlwaysTrue() throws Exception {

		final EasyBooleanFiltering filtering = instantiate(EasyBooleanFilterings.class) //
				.alwaysTrue();

		assertTrue(match(null, filtering));

		assertEquals("true", filtering.toString());

		assertSame(TRUE, filtering.getType());
		assertThat(filtering, instanceOf(AlwaysTrue.class));
	}

	@Test
	public void testAlwaysFalse() throws Exception {

		final EasyBooleanFiltering filtering = instantiate(EasyBooleanFilterings.class) //
				.alwaysFalse();

		assertFalse(match(null, filtering));

		assertEquals("false", filtering.toString());

		assertSame(FALSE, filtering.getType());
		assertThat(filtering, instanceOf(AlwaysFalse.class));
	}

	@Test
	public void test_not_True() throws Exception {

		final EasyBooleanFiltering filtering = not(instantiate(EasyBooleanFilterings.class) //
				.alwaysTrue());

		assertFalse(match(null, filtering));

		assertEquals("not(true)", filtering.toString());

		assertSame(NOT, filtering.getType());
		assertThat(filtering, instanceOf(Not.class));
		assertSame(TRUE, ((Not<?>) filtering).getArg().getType());
		assertThat(((Not<?>) filtering).getArg(), instanceOf(AlwaysTrue.class));
	}

	@Test
	public void test_not_False() throws Exception {

		final EasyBooleanFiltering filtering = not(instantiate(EasyBooleanFilterings.class) //
				.alwaysFalse());

		assertTrue(match(null, filtering));

		assertEquals("not(false)", filtering.toString());

		assertSame(NOT, filtering.getType());
		assertThat(filtering, instanceOf(Not.class));
		assertSame(FALSE, ((Not<?>) filtering).getArg().getType());
		assertThat(((Not<?>) filtering).getArg(), instanceOf(AlwaysFalse.class));
	}

	@Test
	public void test_True_and_True() throws Exception {

		final EasyBooleanFiltering filtering = and(instantiate(EasyBooleanFilterings.class) //
				.alwaysTrue(), //
				instantiate(EasyBooleanFilterings.class) //
						.alwaysTrue());

		assertTrue(match(null, filtering));

		assertEquals("(true and true)", filtering.toString());

		assertSame(AND, filtering.getType());
		assertThat(filtering, instanceOf(And.class));
		assertSame(2, ((And<?>) filtering).getArgs().length);
		assertSame(TRUE, ((And<?>) filtering).getArgs()[0].getType());
		assertThat(((And<?>) filtering).getArgs()[0], instanceOf(AlwaysTrue.class));
		assertSame(TRUE, ((And<?>) filtering).getArgs()[1].getType());
		assertThat(((And<?>) filtering).getArgs()[1], instanceOf(AlwaysTrue.class));
	}

	@Test
	public void test_True_or_True() throws Exception {

		final EasyBooleanFiltering filtering = or(instantiate(EasyBooleanFilterings.class) //
				.alwaysTrue(), //
				instantiate(EasyBooleanFilterings.class) //
						.alwaysTrue());

		assertTrue(match(null, filtering));

		assertEquals("(true or true)", filtering.toString());

		assertSame(OR, filtering.getType());
		assertThat(filtering, instanceOf(Or.class));
		assertSame(2, ((Or<?>) filtering).getArgs().length);
		assertSame(TRUE, ((Or<?>) filtering).getArgs()[0].getType());
		assertThat(((Or<?>) filtering).getArgs()[0], instanceOf(AlwaysTrue.class));
		assertSame(TRUE, ((Or<?>) filtering).getArgs()[1].getType());
		assertThat(((Or<?>) filtering).getArgs()[1], instanceOf(AlwaysTrue.class));
	}

	@Test
	public void test_False_and_False() throws Exception {

		final EasyBooleanFiltering filtering = and(instantiate(EasyBooleanFilterings.class) //
				.alwaysFalse(), //
				instantiate(EasyBooleanFilterings.class) //
						.alwaysFalse());

		assertFalse(match(null, filtering));

		assertEquals("(false and false)", filtering.toString());

		assertSame(AND, filtering.getType());
		assertThat(filtering, instanceOf(And.class));
		assertSame(2, ((And<?>) filtering).getArgs().length);
		assertSame(FALSE, ((And<?>) filtering).getArgs()[0].getType());
		assertThat(((And<?>) filtering).getArgs()[0], instanceOf(AlwaysFalse.class));
		assertSame(FALSE, ((And<?>) filtering).getArgs()[1].getType());
		assertThat(((And<?>) filtering).getArgs()[1], instanceOf(AlwaysFalse.class));
	}

	@Test
	public void test_False_or_False() throws Exception {

		final EasyBooleanFiltering filtering = or(instantiate(EasyBooleanFilterings.class) //
				.alwaysFalse(), //
				instantiate(EasyBooleanFilterings.class) //
						.alwaysFalse());

		assertFalse(match(null, filtering));

		assertEquals("(false or false)", filtering.toString());

		assertSame(OR, filtering.getType());
		assertThat(filtering, instanceOf(Or.class));
		assertSame(2, ((Or<?>) filtering).getArgs().length);
		assertSame(FALSE, ((Or<?>) filtering).getArgs()[0].getType());
		assertThat(((Or<?>) filtering).getArgs()[0], instanceOf(AlwaysFalse.class));
		assertSame(FALSE, ((Or<?>) filtering).getArgs()[1].getType());
		assertThat(((Or<?>) filtering).getArgs()[1], instanceOf(AlwaysFalse.class));
	}

	@Test
	public void test_True_and_False() throws Exception {

		assertFalse(match(null, //
				and(instantiate(EasyBooleanFilterings.class) //
						.alwaysTrue(), //
						instantiate(EasyBooleanFilterings.class) //
								.alwaysFalse())));
	}

	@Test
	public void test_True_or_False() throws Exception {

		assertTrue(match(null, //
				or(instantiate(EasyBooleanFilterings.class) //
						.alwaysTrue(), //
						instantiate(EasyBooleanFilterings.class) //
								.alwaysFalse())));
	}

	@Test
	public void test_False_and_True() throws Exception {

		assertFalse(match(null, //
				and(instantiate(EasyBooleanFilterings.class) //
						.alwaysFalse(), //
						instantiate(EasyBooleanFilterings.class) //
								.alwaysTrue())));
	}

	@Test
	public void test_False_or_True() throws Exception {

		assertTrue(match(null, //
				or(instantiate(EasyBooleanFilterings.class) //
						.alwaysFalse(), //
						instantiate(EasyBooleanFilterings.class) //
								.alwaysTrue())));
	}

	private interface MyComplexFiltering extends Filtering<MyComplexFiltering.Field> {

		enum Field implements Filtering.Field {

			// @Spec(type = String.class)
			NAME,

			// @Spec(type = boolean.class)
			ENABLED,

			@Spec(type = Gender.class)
			GENDER,

			@Spec(type = Gender.class, propertyName = "gendername")
			GENDER2,
		}
	}

	private enum Gender {

		MALE, FEMALE,
	}

	private interface MyComplexFilterings extends Filterings<MyComplexFiltering, MyComplexFiltering.Field> {

		@Override
		MyComplexFiltering eq(MyComplexFiltering.Field field, String value);

		@Override
		MyComplexFiltering eq(MyComplexFiltering.Field field, boolean value);
	}

	@Test
	public void test_NAME_eq_Toto() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.NAME, "Toto");

		assertTrue(match("Toto", filtering));
		assertFalse(match("Tutu", filtering));
		assertFalse(match(null, filtering));

		assertEquals("name eq \"Toto\"", filtering.toString());

		assertSame(EQ, filtering.getType());
		assertThat(filtering, instanceOf(Comparison.class));
		assertSame(MyComplexFiltering.Field.NAME, ((Comparison<?>) filtering).getField());
		assertEquals("Toto", ((Comparison<?>) filtering).getArg());
	}

	@Test
	public void test_NAME_eq_null() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.NAME, (String) null);

		assertFalse(match("Toto", filtering));
		assertFalse(match("Tutu", filtering));
		assertTrue(match(null, filtering));

		assertEquals("name eq null", filtering.toString());

		assertSame(EQ, filtering.getType());
		assertThat(filtering, instanceOf(Comparison.class));
		assertSame(MyComplexFiltering.Field.NAME, ((Comparison<?>) filtering).getField());
		assertEquals(null, ((Comparison<?>) filtering).getArg());
	}

	@Test
	public void test_ENABLED_eq_True() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.ENABLED, true);

		assertTrue(match(new MyComplexBean("XXX", true), filtering));
		assertFalse(match(new MyComplexBean("XXX", false), filtering));

		assertEquals("enabled eq true", filtering.toString());

		assertSame(EQ, filtering.getType());
		assertThat(filtering, instanceOf(Comparison.class));
		assertSame(MyComplexFiltering.Field.ENABLED, ((Comparison<?>) filtering).getField());
		assertEquals(true, ((Comparison<?>) filtering).getArg());
	}

	@Test
	public void test_GENDER_eq_MALE_Enum() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.GENDER, Gender.MALE);

		assertTrue(match(new MyComplexBeanWithGender(Gender.MALE), filtering));
		assertFalse(match(new MyComplexBeanWithGender(Gender.FEMALE), filtering));

		assertEquals("gender eq MALE", filtering.toString());

		assertSame(EQ, filtering.getType());
		assertThat(filtering, instanceOf(Comparison.class));
		assertSame(MyComplexFiltering.Field.GENDER, ((Comparison<?>) filtering).getField());
		assertEquals(Gender.MALE, ((Comparison<?>) filtering).getArg());
	}

	@Test
	public void test_GENDERNAME_eq_MALE_String() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.GENDER2, "MALE");

		assertTrue(match(new MyComplexBeanWithGenderName(Gender.MALE), filtering));
		assertFalse(match(new MyComplexBeanWithGenderName(Gender.FEMALE), filtering));

		assertEquals("gender2 eq \"MALE\"", filtering.toString());

		assertSame(EQ, filtering.getType());
		assertThat(filtering, instanceOf(Comparison.class));
		assertSame(MyComplexFiltering.Field.GENDER2, ((Comparison<?>) filtering).getField());
		assertEquals("MALE", ((Comparison<?>) filtering).getArg());
	}

	@Test
	public void test_GENDERNAME_eq_MALE_Enum() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.GENDER2, Gender.MALE);

		assertTrue(match(new MyComplexBeanWithGenderName(Gender.MALE), filtering));
		assertFalse(match(new MyComplexBeanWithGenderName(Gender.FEMALE), filtering));

		assertEquals("gender2 eq MALE", filtering.toString());

		assertSame(EQ, filtering.getType());
		assertThat(filtering, instanceOf(Comparison.class));
		assertSame(MyComplexFiltering.Field.GENDER2, ((Comparison<?>) filtering).getField());
		assertEquals(Gender.MALE, ((Comparison<?>) filtering).getArg());
	}

	@Test
	public void test_GENDER_eq_MALE_String() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.GENDER, "MALE");

		assertTrue(match(new MyComplexBeanWithGender(Gender.MALE), filtering));
		assertFalse(match(new MyComplexBeanWithGender(Gender.FEMALE), filtering));

		assertEquals("gender eq \"MALE\"", filtering.toString());
	}

	@Test
	public void test_ENABLED_eq_False() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.ENABLED, false);

		assertFalse(match(new MyComplexBean("XXX", true), filtering));
		assertTrue(match(new MyComplexBean("XXX", false), filtering));

		assertEquals("enabled eq false", filtering.toString());
	}

	private static class MyComplexBean {

		private final String name;
		private final boolean enabled;

		public MyComplexBean(final String name, final boolean enabled) {

			this.name = checkNotNull(name, "name");
			this.enabled = enabled;
		}

		@SuppressWarnings("unused")
		public String getName() {

			return name;
		}

		@SuppressWarnings("unused")
		public boolean isEnabled() {

			return enabled;
		}
	}

	private static class MyComplexBeanWithGender {

		private final Gender gender;

		public MyComplexBeanWithGender(final Gender gender) {

			this.gender = checkNotNull(gender, "gender");
		}

		@SuppressWarnings("unused")
		public Gender getGender() {

			return gender;
		}
	}

	private static class MyComplexBeanWithGenderName {

		private final Gender gender;

		public MyComplexBeanWithGenderName(final Gender gender) {

			this.gender = checkNotNull(gender, "gender");
		}

		@SuppressWarnings("unused")
		public String getGendername() {

			return gender.name();
		}
	}

	@Test
	public void testEquals_name_eq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.NAME, "toto"),

				filterings.eq(MyComplexFiltering.Field.NAME, "toto"));

		assertNotEquals(filterings.eq(MyComplexFiltering.Field.NAME, "toto"),

				filterings.eq(MyComplexFiltering.Field.NAME, "tata"));
	}

	@Test
	public void testEquals_name_eq_xxx_enabled_eq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertNotEquals(filterings.eq(MyComplexFiltering.Field.NAME, "toto"),

				filterings.eq(MyComplexFiltering.Field.ENABLED, true));

		assertNotEquals(filterings.eq(MyComplexFiltering.Field.ENABLED, true),

				filterings.eq(MyComplexFiltering.Field.NAME, "toto"));
	}

	@Test
	public void testEquals_enabled_eq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.ENABLED, true),

				filterings.eq(MyComplexFiltering.Field.ENABLED, true));

		assertEquals(filterings.eq(MyComplexFiltering.Field.ENABLED, false),

				filterings.eq(MyComplexFiltering.Field.ENABLED, false));

		assertNotEquals(filterings.eq(MyComplexFiltering.Field.ENABLED, true),

				filterings.eq(MyComplexFiltering.Field.ENABLED, false));
	}
}
