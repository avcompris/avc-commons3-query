package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.impl.FilteringsFactory.and;
import static net.avcompris.commons.query.impl.FilteringsFactory.instantiate;
import static net.avcompris.commons.query.impl.FilteringsFactory.or;
import static org.joda.time.DateTimeZone.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;

import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.Filterings;

public class SqlWhereClauseTest {

	@Test
	public void test_null_Filtering() throws Exception {

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(null, EasyBooleanFiltering.Field.class);

		assertEquals("", sqlWhereClause.getSQL(" WHERE"));
	}

	private interface EasyBooleanFiltering extends Filtering<EasyBooleanFiltering.Field> {

		enum Field implements Filtering.Field {

		}
	}

	private interface EasyBooleanFilterings extends Filterings<EasyBooleanFiltering, EasyBooleanFiltering.Field> {

		@Override
		EasyBooleanFiltering alwaysTrue();

		@Override
		EasyBooleanFiltering alwaysFalse();
	}

	@Test
	public void test_empty_alwaysTrue() throws Exception {

		final EasyBooleanFilterings filterings = instantiate(EasyBooleanFilterings.class);

		final Filtering<EasyBooleanFiltering.Field> filtering = filterings.alwaysTrue();

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE 1", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_empty_alwaysFalse() throws Exception {

		final EasyBooleanFilterings filterings = instantiate(EasyBooleanFilterings.class);
		final Filtering<EasyBooleanFiltering.Field> filtering = filterings.alwaysFalse();

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE 0", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_empty_not_True() throws Exception {

		final EasyBooleanFilterings filterings = instantiate(EasyBooleanFilterings.class);
		final Filtering<EasyBooleanFiltering.Field> filtering = FilteringsFactory.not(filterings.alwaysTrue());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE NOT(1)", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_empty_not_False() throws Exception {

		final EasyBooleanFilterings filterings = instantiate(EasyBooleanFilterings.class);
		final Filtering<EasyBooleanFiltering.Field> filtering = FilteringsFactory.not(filterings.alwaysFalse());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE NOT(0)", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_empty_true_and_true() throws Exception {

		final EasyBooleanFilterings filterings = instantiate(EasyBooleanFilterings.class);
		final Filtering<EasyBooleanFiltering.Field> filtering = and( //
				filterings.alwaysTrue(), //
				filterings.alwaysTrue());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE (1) AND (1)", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_empty_true_and_false() throws Exception {

		final Filtering<EasyBooleanFiltering.Field> filtering = and( //
				instantiate(EasyBooleanFilterings.class).alwaysTrue(),
				instantiate(EasyBooleanFilterings.class).alwaysFalse());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" AND (1) AND (0)", sqlWhereClause.getSQL(" AND"));
	}

	@Test
	public void test_empty_false_and_true() throws Exception {

		final Filtering<EasyBooleanFiltering.Field> filtering = and( //
				instantiate(EasyBooleanFilterings.class).alwaysFalse(),
				instantiate(EasyBooleanFilterings.class).alwaysTrue());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE (0) AND (1)", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_empty_false_and_false() throws Exception {

		final Filtering<EasyBooleanFiltering.Field> filtering = and( //
				instantiate(EasyBooleanFilterings.class).alwaysFalse(),
				instantiate(EasyBooleanFilterings.class).alwaysFalse());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE (0) AND (0)", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_empty_true_or_true() throws Exception {

		final Filtering<EasyBooleanFiltering.Field> filtering = or( //
				instantiate(EasyBooleanFilterings.class).alwaysTrue(),
				instantiate(EasyBooleanFilterings.class).alwaysTrue());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE (1) OR (1)", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_empty_true_or_false() throws Exception {

		final Filtering<EasyBooleanFiltering.Field> filtering = or( //
				instantiate(EasyBooleanFilterings.class).alwaysTrue(),
				instantiate(EasyBooleanFilterings.class).alwaysFalse());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" AND (1) OR (0)", sqlWhereClause.getSQL(" AND"));
	}

	@Test
	public void test_empty_false_or_true() throws Exception {

		final Filtering<EasyBooleanFiltering.Field> filtering = or( //
				instantiate(EasyBooleanFilterings.class).alwaysFalse(),
				instantiate(EasyBooleanFilterings.class).alwaysTrue());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE (0) OR (1)", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_empty_false_or_false() throws Exception {

		final Filtering<EasyBooleanFiltering.Field> filtering = or( //
				instantiate(EasyBooleanFilterings.class).alwaysFalse(),
				instantiate(EasyBooleanFilterings.class).alwaysFalse());

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, EasyBooleanFiltering.Field.class);

		assertEquals(" WHERE (0) OR (0)", sqlWhereClause.getSQL(" WHERE"));
	}

	private interface MyComplexFiltering extends Filtering<MyComplexFiltering.Field> {

		enum Field implements Filtering.Field {

			@Spec(type = String.class, sqlName = "name")
			NAME,

			@Spec(type = String.class, sqlName = "name2")
			OTHER_NAME,

			@Spec(type = boolean.class, sqlName = "tall")
			IS_TALL,

			@Spec(type = DateTime.class, sqlName = "created_at")
			CREATED_AT,
		}

	}

	private interface MyComplexFilterings extends Filterings<MyComplexFiltering, MyComplexFiltering.Field> {

		@Override
		MyComplexFiltering eq(MyComplexFiltering.Field field, String value);
	}

	@Test
	public void test_NAME_eq_Toto() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.NAME, "Toto");

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, MyComplexFiltering.Field.class);

		assertEquals(" WHERE name = 'Toto'", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_NAME_eq_Toto_prefix() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.NAME, "Toto");

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, MyComplexFiltering.Field.class, "p.");

		assertEquals(" WHERE p.name = 'Toto'", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_OTHER_NAME_eq_Toto() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.OTHER_NAME, "Toto");

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, MyComplexFiltering.Field.class);

		assertEquals(" WHERE name2 = 'Toto'", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_TALL_eq_True() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.IS_TALL, true);

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, MyComplexFiltering.Field.class);

		assertEquals(" WHERE tall = TRUE", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_TALL_eq_False() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.IS_TALL, false);

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, MyComplexFiltering.Field.class);

		assertEquals(" WHERE tall = FALSE", sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_CREATED_AT_eq_2020_01_11_09_34_56_111() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.eq(MyComplexFiltering.Field.CREATED_AT, new DateTime(2020, 1, 11, 9, 34, 56, 111, UTC));

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, MyComplexFiltering.Field.class);

		assertEquals(
				" WHERE DATE_TRUNC('milliseconds', created_at) = TO_TIMESTAMP('2020-01-11 09:34:56.111', 'YYYY-MM-DD HH24:MI:SS.MS')",
				sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void test_CREATED_AT_gt_2020_01_11_09_34_56_111() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.gt(MyComplexFiltering.Field.CREATED_AT, new DateTime(2020, 1, 11, 9, 34, 56, 111, UTC));

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, MyComplexFiltering.Field.class);

		assertEquals(
				" WHERE DATE_TRUNC('milliseconds', created_at) > TO_TIMESTAMP('2020-01-11 09:34:56.111', 'YYYY-MM-DD HH24:MI:SS.MS')",
				sqlWhereClause.getSQL(" WHERE"));
	}

	@Test
	public void testParse_name_contains_xxx_percent_start() throws Exception {

		final Filtering<MyComplexFiltering.Field> filtering = instantiate(MyComplexFilterings.class) //
				.parse("NAME CONTAINS %m");

		final SqlWhereClause sqlWhereClause = SqlWhereClause.build(filtering, MyComplexFiltering.Field.class);

		assertEquals("name LIKE '%\\%m%'",

				sqlWhereClause.getSQL(""));
	}
}
