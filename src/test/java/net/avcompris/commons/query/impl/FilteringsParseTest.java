package net.avcompris.commons.query.impl;

import static net.avcompris.commons.query.impl.FilteringsFactory.and;
import static net.avcompris.commons.query.impl.FilteringsFactory.instantiate;
import static net.avcompris.commons.query.impl.FilteringsFactory.not;
import static net.avcompris.commons.query.impl.FilteringsFactory.or;
import static org.joda.time.DateTimeZone.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;

import net.avcompris.commons.query.FilterSyntaxException;
import net.avcompris.commons.query.Filtering;
import net.avcompris.commons.query.Filterings;

public class FilteringsParseTest {

	private enum Gender {

		MALE, FEMALE,
	}

	private enum Type {

		A, B, C,
	}

	private interface MyComplexFiltering extends Filtering<MyComplexFiltering.Field> {

		enum Field implements Filtering.Field {

			@Spec(type = String.class, alias = "myname")
			NAME,

			@Spec(type = Gender.class)
			GENDER,

			@Spec(type = FilteringsParseTest.Type.class)
			TYPE,

			@Spec(type = int.class)
			AGE,

			@Spec(type = boolean.class)
			ENABLED,

			@Spec(type = DateTime.class)
			LAST_SEEN_AT,
		}
	}

	private interface MyComplexFilterings extends Filterings<MyComplexFiltering, MyComplexFiltering.Field> {

		@Override
		MyComplexFiltering parse(String expression) throws FilterSyntaxException;

		@Override
		MyComplexFiltering eq(MyComplexFiltering.Field field, String value);

		@Override
		MyComplexFiltering eq(MyComplexFiltering.Field field, boolean value);

		// @Override
		MyComplexFiltering eq(MyComplexFiltering.Field field, Gender value);
	}

	@Test
	public void testParse_name_eq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.NAME, "toto"),

				filterings.parse("name eq toto"));
	}

	@Test
	public void testParse_alias_eq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.NAME, "toto"),

				filterings.parse("myname eq toto"));
	}

	@Test
	public void testParse_name_eq_xxx_quotes() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.NAME, "toto"),

				filterings.parse("name eq \"toto\""));
	}

	@Test
	public void testParse_name_eq_xxx_simpleQuotes() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.NAME, "toto"),

				filterings.parse("name eq 'toto'"));
	}

	@Test
	public void testParse_name_eq_xxx_quotedParenthesis() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.NAME, "(toto)"),

				filterings.parse("name eq '(toto)'"));
	}

	@Test
	public void testParse_name_eq_xxx_unquotedParenthesis() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertThrows(FilterSyntaxException.class, ()

		-> filterings.parse("name eq (toto)"));
	}

	@Test
	public void testParse_name_eq_null() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.NAME, (String) null),

				filterings.parse("name eq null"));
	}

	@Test
	public void testParse_name_eq_null_quotes() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.NAME, "null"),

				filterings.parse("name eq \"null\""));
	}

	@Test
	public void testParse_name_eq_null_simpleQuotes() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.NAME, "null"),

				filterings.parse("name eq 'null'"));
	}

	@Test
	public void testParse_not2_gender_eq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(not(filterings.eq(MyComplexFiltering.Field.GENDER, Gender.MALE)),

				filterings.parse("!(gender eq MALE)"));
	}

	@Test
	public void testParse_age_20_or_age_50() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(or( //
				filterings.eq(MyComplexFiltering.Field.AGE, 20), //
				filterings.eq(MyComplexFiltering.Field.AGE, 50)),

				filterings.parse("age = 20 or age = 50"));
	}

	@Test
	public void testParse_age_20_or_age_50_notEquals_and() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertNotEquals(and( //
				filterings.eq(MyComplexFiltering.Field.AGE, 20), //
				filterings.eq(MyComplexFiltering.Field.AGE, 50)),

				filterings.parse("age = 20 or age = 50"));
	}

	@Test
	public void testParse_age_neq_20_and_age_neq_50() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(and( //
				filterings.neq(MyComplexFiltering.Field.AGE, 20), //
				filterings.neq(MyComplexFiltering.Field.AGE, 50)),

				filterings.parse("age != 20 and age neq 50"));
	}

	@Test
	public void testParse_type_a_and_type_b_and_type_c_parenthesis_0_1_0() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(and( //
				filterings.eq(MyComplexFiltering.Field.TYPE, Type.A), //
				filterings.eq(MyComplexFiltering.Field.TYPE, Type.B), //
				filterings.eq(MyComplexFiltering.Field.TYPE, Type.C)),

				filterings.parse("type = a and (type = b) and type = c"));
	}

	@Test
	public void testParse_type_x_and_type_y_and_type_z_parenthesis_0_2_0() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(and( //
				filterings.eq(MyComplexFiltering.Field.TYPE, "x"), //
				filterings.eq(MyComplexFiltering.Field.TYPE, "y"), //
				filterings.eq(MyComplexFiltering.Field.TYPE, "z")),

				filterings.parse("type = x and ((type = y) and type = z)"));
	}

	@Test
	public void testParse_name_contains_xxx_UPPERCASE() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.contains(MyComplexFiltering.Field.NAME, "wiQDLLgVn3p2Hsi1PfPJ"),

				filterings.parse("NAME CONTAINS wiQDLLgVn3p2Hsi1PfPJ"));
	}

	@Test
	public void testParse_last_seen_at_gt2_xxx_quotes() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		final DateTime ref = new DateTime().withZone(UTC);

		assertEquals(filterings.gt(MyComplexFiltering.Field.LAST_SEEN_AT, ref),

				filterings.parse("last_seen_at > \"" + ref + "\""));
	}

	@Test
	public void testParse_name_eq2_xxx_and_last_seen_at_eq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(and( //
				filterings.eq(MyComplexFiltering.Field.NAME, "toto"), //
				filterings.eq(MyComplexFiltering.Field.LAST_SEEN_AT, "2010-03-04")),

				filterings.parse("name == toto and last_seen_at eq 2010-03-04"));
	}

	@Test
	public void testParse_lastSeenAt_eq_dateTimeWithMillisecondsZone() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(
				filterings.eq(MyComplexFiltering.Field.LAST_SEEN_AT, new DateTime(2010, 03, 04, 12, 34, 56, 123, UTC)),

				filterings.parse("lastSeenAt = 2010-03-04T12:34:56.123Z"));
	}

	@Test
	public void testParse_not2_type_eq_xxx_and_type_neq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(and( //
				not(filterings.eq(MyComplexFiltering.Field.TYPE, Type.A)), //
				filterings.neq(MyComplexFiltering.Field.TYPE, Type.B)),

				filterings.parse("!(type eq A) && type != B"));
	}

	@Test
	public void testParse_lastSeenAt_eq_illegalDate() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertThrows(FilterSyntaxException.class, ()

		-> filterings.parse("last_seen_at=2019-12-35"));
	}

	@Test
	public void testParse_not2_type_eq_xxx_or_type_neq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(or( //
				not(filterings.eq(MyComplexFiltering.Field.TYPE, Type.A)), //
				filterings.neq(MyComplexFiltering.Field.TYPE, Type.B)),

				filterings.parse("!(type eq A) || type != B"));
	}

	@Test
	public void testParse_not2_type_eq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(not(filterings.eq(MyComplexFiltering.Field.TYPE, Type.A)),

				filterings.parse("!(type eq A)"));
	}

	@Test
	public void testParse_type_neq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.neq(MyComplexFiltering.Field.TYPE, Type.B),

				filterings.parse("type != B"));
	}

	@Test
	public void testParse_type_eq_xxx() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(filterings.eq(MyComplexFiltering.Field.TYPE, Type.B),

				filterings.parse("type = B"));
	}

	@Test
	public void testParse_lastSeendAt_eq_dateTimeWithMilliseconds() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(
				filterings.eq(MyComplexFiltering.Field.LAST_SEEN_AT, new DateTime(2010, 03, 04, 12, 34, 56, 431, UTC)),

				filterings.parse("lastSeenAt = 2010-03-04T12:34:56.431"));
	}

	@Test
	public void testParse_name_EQ_xxx_and_lastSeenAt_NEQ_xxx_parenthesis() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(and( //
				filterings.eq(MyComplexFiltering.Field.NAME, "toto"), //
				filterings.neq(MyComplexFiltering.Field.LAST_SEEN_AT, "2010-03-04")),

				filterings.parse("(name EQ toto) and (lastSeenAt NEQ \"2010-03-04\")"));
	}

	@Test
	public void testParse_name_EQ_xxx_or_lastSeenAt_NEQ_xxx_parenthesis() throws Exception {

		final MyComplexFilterings filterings = instantiate(MyComplexFilterings.class);

		assertEquals(or( //
				filterings.eq(MyComplexFiltering.Field.NAME, "toto"), //
				filterings.neq(MyComplexFiltering.Field.LAST_SEEN_AT, "2010-03-04")),

				filterings.parse("(name EQ toto) or (lastSeenAt NEQ \"2010-03-04\")"));
	}
}
