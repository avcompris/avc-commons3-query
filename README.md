# About avc-commons3-query

Query framework for avc-commons3 projects.


[API Documentation is here](https://maven.avcompris.com/avc-commons3-query/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-commons3-query/)
